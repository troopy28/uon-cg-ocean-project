// AssetMaker.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#define FBXSDK_SHARED
#include <fbxsdk.h>
#include <unordered_set>
#include <lz4/lz4.h>
#include <algorithm>
#include <cctype>
#include <string>
#include <glm/glm.hpp>

#define FILENAMEWITHOUTPATH (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

#define LOG_AND_THROW(message)	do { std::cout << "[error] " << FILENAMEWITHOUTPATH << " > " << __func__ << "[" <<  std::to_string(__LINE__) << "] " <<  (message) << std::endl; \
								throw std::runtime_error(std::string(message)); } while(false)

#define LOG_WARN(message) std::cout << "[warning] " << FILENAMEWITHOUTPATH << " > " << __func__ << "[" <<  std::to_string(__LINE__) << "] " <<  (message) << std::endl

#define LOG_ERR(message)	std::cout << "[error] " << FILENAMEWITHOUTPATH << " > " << __func__ << "[" <<  std::to_string(__LINE__) << "] " <<  (message) << std::endl



namespace fs = std::filesystem;

using IndexT = uint32_t;


struct Vertex
{
	glm::vec3 pos{ 0.0f };        // 0
	glm::vec3 normal{ 0.0f };     // 12
	glm::vec3 tangent{ 0.0f };    // 24
	glm::vec3 bitangent{ 0.0f };  // 36
	glm::vec2 uv{ 0.0f };		    // 48

	// NOLINT(clang-diagnostic-float-equal)
	bool operator==(const Vertex& other) const
	{
		// Note: perform exact float comparisons here, since this operator is used for
		// checking for EXACTNESS of compressed-decompressed and original data
		return other.pos == pos
			&& other.normal == normal
			&& other.tangent == tangent
			&& other.bitangent == bitangent
			&& other.uv == uv;
	}
};

struct Mesh
{
	/**
	 * \brief Individual vertices, no duplicates. Goes straight in OpenGL vertex buffers.
	 */
	std::vector<Vertex> vertices;
	/**
	 * \brief Indices to connect vertices into faces. Goes straight in OpenGL index buffers.
	 */
	std::vector<uint32_t> indices;
};

inline size_t HashCombine(const size_t lhs, const size_t rhs)
{
	return lhs ^ (rhs + 0x9e3779b9 + (lhs << 6) + (lhs >> 2));
}

struct Vector3Hash {
	std::size_t operator () (const Vertex& v) const {
		const auto h1 = std::hash<float>{}(v.pos.x);
		const auto h2 = std::hash<float>{}(v.pos.y);
		const auto h3 = std::hash<float>{}(v.pos.z);
		const auto h4 = std::hash<float>{}(v.uv.x);
		const auto h5 = std::hash<float>{}(v.uv.y);
		return HashCombine(HashCombine(HashCombine(HashCombine(h1, h2), h3), h4), h5);
	}
};

template<typename T>
char* CompressVector(std::vector<T> vec, int& compressedDataSize)
{
	const int sourceDataSize = static_cast<int>(vec.size() * sizeof(T));
	const int maxDestinationSize = LZ4_compressBound(sourceDataSize);
	char* compressedData = static_cast<char*>(malloc(static_cast<size_t>(maxDestinationSize)));
	if (compressedData == nullptr)
		LOG_AND_THROW("Malloc failed");

	const char* sourceData = reinterpret_cast<const char*>(vec.data());
	compressedDataSize = LZ4_compress_default(sourceData, compressedData, sourceDataSize, maxDestinationSize);
	if (compressedDataSize <= 0)
		LOG_AND_THROW("Compression failed");

	return compressedData;
}

/**
 * \brief Decompresses the given data into a vector of the given element type, of the given size.
 * \tparam T Type of the data stored in the vector.
 * \param compressedData Pointer to the compressed data to build the vector from.
 * \param compressedDataSize Size of the compressed array.
 * \param targetVectorSize Number of elements in the vector. This information cannot be retrieved from the data and needs to be provided.
 * \return Vector from the compressed data.
 */
template<typename T>
std::vector<T> DecompressAsVector(char* compressedData, const int compressedDataSize, const size_t targetVectorSize)
{
	std::vector<T> vec(targetVectorSize);
	char* uncompressedDataTarget = reinterpret_cast<char*>(vec.data());
	const int vectorByteSize = static_cast<int>(targetVectorSize * sizeof(T));
	LZ4_decompress_safe(compressedData, uncompressedDataTarget, compressedDataSize, vectorByteSize);
	return vec; // note: copy elision
}



void ReadFbxMesh(const std::string& sourcePath, std::vector<Vertex>& nonUniqueVertices, std::vector<Vertex>& uniqueVertices, std::vector<IndexT>& faceIndices)
{
	FbxManager* sdkManager = FbxManager::Create();
	if (!sdkManager)
	{
		LOG_AND_THROW("Unable to create the FBX SDK manager.");
	}
	FbxIOSettings* ios = FbxIOSettings::Create(sdkManager, IOSROOT);
	sdkManager->SetIOSettings(ios);

	// Get the SDK version.
	int sdkMajor, sdkMinor, sdkRevision;
	FbxManager::GetFileFormatVersion(sdkMajor, sdkMinor, sdkRevision);

	// Import the file and read its FBX version.
	FbxImporter* importer = FbxImporter::Create(sdkManager, "");
	const bool initSuccess = importer->Initialize(sourcePath.c_str(), -1, sdkManager->GetIOSettings());
	int fileMajor, fileMinor, fileRevision;
	importer->GetFileVersion(fileMajor, fileMinor, fileRevision);

	if (!initSuccess)
	{
		FbxString error = importer->GetStatus().GetErrorString();
		LOG_ERR("Call to FbxImporter::Initialize() failed.");
		LOG_ERR(std::string("Error: ") + error.Buffer());
		if (importer->GetStatus().GetCode() == FbxStatus::eInvalidFileVersion)
		{
			LOG_ERR("FBX file format version for this FBX SDK is "
				+ std::to_string(sdkMajor) +
				"." + std::to_string(sdkMinor) +
				"." + std::to_string(sdkRevision) + "\n");
			LOG_ERR("FBX file format version for file " + sourcePath + " is "
				+ std::to_string(fileMajor) +
				"." + std::to_string(fileMinor) +
				"." + std::to_string(fileRevision) + "\n");
		}
		LOG_AND_THROW("Cannot initialize importer for FBX file");
	}
	std::cout << "File FBX version: " << fileMajor << "." << fileMinor << "." << fileRevision << " \n";


	// FINALLY FUCKING IMPORT THE SCENE.
	FbxScene* fbxScene = FbxScene::Create(sdkManager, "FbxImportScene");
	const bool importSuccess = importer->Import(fbxScene);
	if (!importSuccess)
	{
		LOG_WARN("FBX Importer Error:" + std::string(importer->GetStatus().GetErrorString()) + "\n");
		LOG_AND_THROW("Cannot import FBX file");
	}

	// Check scene integrity...
	FbxStatus status;
	FbxArray< FbxString*> details;
	FbxSceneCheckUtility sceneCheck(FbxCast<FbxScene>(fbxScene), &status, &details);
	const bool validScene = sceneCheck.Validate(FbxSceneCheckUtility::eCkeckData);
	const bool notify = (!validScene && details.GetCount() > 0) || (importer->GetStatus().GetCode() != FbxStatus::eSuccess);
	if (notify)
	{
		if (details.GetCount())
		{
			LOG_WARN("FBX Scene integrity verification failed with the following errors:");
			for (int i = 0; i < details.GetCount(); i++)
				LOG_WARN(details[i]->Buffer());

			FbxArrayDelete<FbxString*>(details);
		}
		if (importer->GetStatus().GetCode() != FbxStatus::eSuccess)
		{
			LOG_WARN("   The importer was able to read the file but with errors.\n");
			LOG_WARN("   Loaded scene may be incomplete.\n\n");
			LOG_WARN("   Last error message:" + std::string(importer->GetStatus().GetErrorString()) + "\n");
		}
	}

	FbxNode* rootNode = fbxScene->GetRootNode();
	if (!rootNode)
	{
		LOG_WARN("FBX Scene has no root node. The scene is empty. This might be normal if you gave a path to an empty fbx file.");
		return;
	}

	std::vector<FbxNode*> iterationStack{ rootNode };
	std::vector<FbxMesh*> meshes;

	while (!iterationStack.empty())
	{
		FbxNode* node = iterationStack.back();
		iterationStack.pop_back();
		const FbxNodeAttribute* attribute = node->GetNodeAttribute();
		if (attribute != nullptr)
		{
			const FbxNodeAttribute::EType attributeType = node->GetNodeAttribute()->GetAttributeType();
			if (attributeType == FbxNodeAttribute::eMesh)
			{
				meshes.emplace_back(static_cast<FbxMesh*>(node->GetNodeAttribute()));  // NOLINT(cppcoreguidelines-pro-type-static-cast-downcast)
			}
		}

		for (int i = 0; i < node->GetChildCount(); i++)
		{
			FbxNode* child = node->GetChild(i);
			if (child != nullptr)
			{
				iterationStack.emplace_back(child);
			}
		}
	}

	/*************************************************************/
	/*                ACTUAL READING STARTS HERE.                */
	/*************************************************************/

	nonUniqueVertices.clear();
	faceIndices.clear();

	int offset = 0;
	for (FbxMesh* fbxMesh : meshes)
	{
		// Read vertices.
		const int controlPointsCount = fbxMesh->GetControlPointsCount();
		const FbxVector4* controlPoints = fbxMesh->GetControlPoints();

		for (int i = 0; i < controlPointsCount; i++)
		{
			const FbxVector4 position = controlPoints[i];
			uniqueVertices.emplace_back(
				Vertex{
					{
						static_cast<float>(position[0]),
						static_cast<float>(position[1]),
						static_cast<float>(position[2])
					},
					{0.0f, 0.0f, 0.0f},
					{0.0f, 0.0f, 0.0f},
					{0.0f, 0.0f, 0.0f},
					{0.0f, 0.0f}
				});
		}

		// Get the UV layer to read from.
		FbxStringList uvSetNameList;
		fbxMesh->GetUVSetNames(uvSetNameList);
		bool hasUvs = uvSetNameList.GetCount() > 0;
		const char* uvSetName = hasUvs ? uvSetNameList.GetStringAt(0) : nullptr;
		const FbxGeometryElementUV* uvElement = hasUvs ? fbxMesh->GetElementUV(uvSetName) : nullptr;
		if (uvElement == nullptr || (uvElement->GetMappingMode() != FbxGeometryElement::eByPolygonVertex &&
			uvElement->GetMappingMode() != FbxGeometryElement::eByControlPoint))
			hasUvs = false;

		// Get the normals element to read from.
		bool hasNormals = fbxMesh->GetElementNormalCount() > 0;
		const FbxGeometryElementNormal* normalElement = hasNormals ? fbxMesh->GetElementNormal(0) : nullptr;
		if (normalElement == nullptr || (normalElement->GetMappingMode() != FbxGeometryElement::eByPolygonVertex &&
			normalElement->GetMappingMode() != FbxGeometryElement::eByControlPoint))
			hasNormals = false;


		// Read face indices and UVs.
		const int polygonCount = fbxMesh->GetPolygonCount();
		for (int i = 0; i < polygonCount; i++)
		{
			const int polygonSize = fbxMesh->GetPolygonSize(i);
			if (polygonSize != 3)
			{
				LOG_AND_THROW("FBX FILE CONTAINS A POLYGON WITH " + std::to_string(polygonSize) + ". This is NOT SUPPORTED. The importer only supports triangle meshes.");
			}
			for (int j = 0; j < polygonSize; j++)
			{
				const int controlPointIndex = fbxMesh->GetPolygonVertex(i, j);
				if (controlPointIndex < 0)
				{
					LOG_WARN("Invalid index encountered: " + std::to_string(controlPointIndex) + ". Skipping the polygon.");
					continue;
				}
				if (hasUvs)
				{
					const FbxLayerElement::EMappingMode mappingMode = uvElement->GetMappingMode();
					const FbxLayerElement::EReferenceMode referenceMode = uvElement->GetReferenceMode();
					if (mappingMode == FbxGeometryElement::eByControlPoint)
					{
						if (referenceMode == FbxLayerElement::eDirect)
						{
							const FbxVector2 uv = uvElement->GetDirectArray().GetAt(controlPointIndex);
							uniqueVertices[controlPointIndex].uv.x = static_cast<float>(uv[0]);
							uniqueVertices[controlPointIndex].uv.y = 1.0f - static_cast<float>(uv[1]);
						}
						else if (referenceMode == FbxLayerElement::eIndexToDirect)
						{
							const int id = uvElement->GetIndexArray().GetAt(controlPointIndex);
							const FbxVector2 uv = uvElement->GetDirectArray().GetAt(id);
							uniqueVertices[controlPointIndex].uv.x = static_cast<float>(uv[0]);
							uniqueVertices[controlPointIndex].uv.y = 1.0f - static_cast<float>(uv[1]);
						}
					}
					else if (mappingMode == FbxLayerElement::eByPolygonVertex)
					{
						const int textureUVIndex = fbxMesh->GetTextureUVIndex(i, j);
						const FbxVector2 uv = uvElement->GetDirectArray().GetAt(textureUVIndex);
						uniqueVertices[controlPointIndex].uv.x = static_cast<float>(uv[0]);
						uniqueVertices[controlPointIndex].uv.y = 1.0f - static_cast<float>(uv[1]);
					}
				}
				if (hasNormals)
				{
					const FbxLayerElement::EMappingMode mappingMode = normalElement->GetMappingMode();
					const FbxLayerElement::EReferenceMode referenceMode = normalElement->GetReferenceMode();
					if (mappingMode == FbxGeometryElement::eByControlPoint)
					{
						if (referenceMode == FbxLayerElement::eDirect)
						{
							const FbxVector4 normal = normalElement->GetDirectArray().GetAt(controlPointIndex);
							uniqueVertices[controlPointIndex].normal.x = static_cast<float>(normal[0]);
							uniqueVertices[controlPointIndex].normal.y = static_cast<float>(normal[1]);
							uniqueVertices[controlPointIndex].normal.z = static_cast<float>(normal[2]);
						}
						else if (referenceMode == FbxLayerElement::eIndexToDirect)
						{
							const int id = normalElement->GetIndexArray().GetAt(controlPointIndex);
							const FbxVector4 normal = normalElement->GetDirectArray().GetAt(id);
							uniqueVertices[controlPointIndex].normal.x = static_cast<float>(normal[0]);
							uniqueVertices[controlPointIndex].normal.y = static_cast<float>(normal[1]);
							uniqueVertices[controlPointIndex].normal.z = static_cast<float>(normal[2]);
						}
					}
					else if (mappingMode == FbxLayerElement::eByPolygonVertex)
					{
						const int textureUVIndex = fbxMesh->GetTextureUVIndex(i, j);
						const FbxVector4 normal = normalElement->GetDirectArray().GetAt(textureUVIndex);
						uniqueVertices[controlPointIndex].normal.x = static_cast<float>(normal[0]);
						uniqueVertices[controlPointIndex].normal.y = static_cast<float>(normal[1]);
						uniqueVertices[controlPointIndex].normal.z = static_cast<float>(normal[2]);
					}
				}
				faceIndices.emplace_back(offset + static_cast<IndexT>(controlPointIndex));
			}
		}

		offset += controlPointsCount;
	}

	// Then build the array of non-indexed vertices.
	for(const IndexT idx : faceIndices)
	{
		nonUniqueVertices.push_back(uniqueVertices[idx]);
	}

	importer->Destroy();
	sdkManager->Destroy();
}


// Directly adapted from https://www.opengl-tutorial.org/intermediate-tutorials/tutorial-13-normal-mapping/#normal-textures
void ComputeTangentBasis(std::vector<Vertex>& allVertices)
{
	const size_t vertexCount = allVertices.size();
	for (size_t i = 0; i < vertexCount; i += 3) 
	{
		// Shortcuts for vertices
		const glm::vec3& v0 = allVertices[i + 0].pos;
		const glm::vec3& v1 = allVertices[i + 1].pos;
		const glm::vec3& v2 = allVertices[i + 2].pos;

		// Shortcuts for UVs
		const glm::vec2& uv0 = allVertices[i + 0].uv;
		const glm::vec2& uv1 = allVertices[i + 1].uv;
		const glm::vec2& uv2 = allVertices[i + 2].uv;

		// Edges of the triangle : position delta
		const glm::vec3 deltaPos1 = v1 - v0;
		const glm::vec3 deltaPos2 = v2 - v0;

		// UV delta
		const glm::vec2 deltaUV1 = uv1 - uv0;
		const glm::vec2 deltaUV2 = uv2 - uv0;

		const float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
		const glm::vec3 tangent = (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y) * r;
		const glm::vec3 bitangent = (deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x) * r;

		allVertices[i + 0].tangent = tangent;
		allVertices[i + 1].tangent = tangent;
		allVertices[i + 2].tangent = tangent;

		allVertices[i + 0].bitangent = bitangent;
		allVertices[i + 1].bitangent = bitangent;
		allVertices[i + 2].bitangent = bitangent;
	}

	for (IndexT i = 0; i < allVertices.size(); i += 1)
	{
		const glm::vec3& n = allVertices[i].normal;
		const glm::vec3& b = allVertices[i].bitangent;
		glm::vec3& t = allVertices[i].tangent;

		// Gram-Schmidt orthogonalize
		t = glm::normalize(t - n * glm::dot(n, t));

		// Calculate handedness
		if (glm::dot(glm::cross(n, t), b) < 0.0f) {
			t = t * -1.0f;
		}
	}
}


Mesh MeshFromVertices(const std::vector<Vertex>& nonUniqueVertices, std::vector<Vertex>& uniqueVertices, std::vector<IndexT>& indices)
{
	for(size_t nonUniqueVertexIdx = 0; nonUniqueVertexIdx < nonUniqueVertices.size(); nonUniqueVertexIdx++)
	{
		const IndexT uniqueVertIdx = indices[nonUniqueVertexIdx];
		uniqueVertices[uniqueVertIdx].tangent += nonUniqueVertices[nonUniqueVertexIdx].tangent;
		uniqueVertices[uniqueVertIdx].bitangent += nonUniqueVertices[nonUniqueVertexIdx].bitangent;
	}
	Mesh mesh;
	mesh.vertices = std::move(uniqueVertices);
	mesh.indices = std::move(indices);
	
	return mesh;
}

void ExportGameMesh(const Mesh& mesh, const std::string& targetPath)
{
	// Simply export the file.
	std::ofstream out(targetPath, std::ios::out | std::ios::binary);
	if (!out.is_open())
		LOG_AND_THROW("Could not write into the file " + targetPath);

	const size_t vc = mesh.vertices.size();
	out.write(reinterpret_cast<const char*>(&vc), sizeof(size_t));
	out.write(reinterpret_cast<const char*>(mesh.vertices.data()), static_cast<std::streamsize>(vc * sizeof(Vertex)));

	const size_t ic = mesh.indices.size();
	out.write(reinterpret_cast<const char*>(&ic), sizeof(size_t));
	out.write(reinterpret_cast<const char*>(mesh.indices.data()), static_cast<std::streamsize>(ic * sizeof(uint32_t)));

	std::cout << "Output file successfully written.\n";
}

void ProcessFile(const fs::path& sourcePath, const std::string& destPath)
{
	if (!fs::exists(sourcePath))
	{
		std::cout << "Could not find input mesh file " << sourcePath << std::endl;
		std::quick_exit(1);
	}
	std::cout << "Source mesh: " << sourcePath << std::endl;
	std::cout << "Target mesh: " << destPath << std::endl;

	// 2. Read the source mesh.
	
	std::vector<Vertex> uniqueVertices;
	std::vector<Vertex> nonUniqueVertices;
	std::vector<IndexT> indices;
	const std::string ext = sourcePath.extension().string();
	if (ext == ".fbx")
	{
		ReadFbxMesh(sourcePath.generic_string(), nonUniqueVertices, uniqueVertices, indices);
	}
	else
	{
		std::cout << "Unsupported input file format " << ext << std::endl;
		std::quick_exit(1);
	}
	ComputeTangentBasis(nonUniqueVertices);
	const Mesh mesh = MeshFromVertices(nonUniqueVertices, uniqueVertices, indices);

	// 3. Convert the mesh into.
	ExportGameMesh(mesh, destPath);

	// 4. Print the file of both files and the ratio.
	const float sourceFileSize = static_cast<float>(std::filesystem::file_size(sourcePath)) / 1000000.0f;
	const float targetFileSize = static_cast<float>(std::filesystem::file_size(destPath)) / 1000000.0f;
	const float fileRatio = 100.0f * targetFileSize / sourceFileSize;
	std::cout << "Source file size : " << sourceFileSize << "MB" << std::endl;
	std::cout << "Dest.  file size : " << targetFileSize << "MB" << std::endl;
	std::cout << "File ratio       : " << fileRatio << "% (" << (100.0f - fileRatio) << "% of original size removed)" << std::endl;
}

int main(const int argc, const char** argv)
{
	std::cout << std::fixed << std::setprecision(2); // To print float with two digits after the "."
	std::cout << "AssetMaker command line tool\n";

	try
	{
		// 1. Manage source and target file names.
		if (argc > 1)
		{
			const std::string sourceFileName = argv[1];
			const fs::path sourcePath(sourceFileName);
			std::string destPath;
			if (argc > 2) // User specified source file and dest file.
			{
				destPath = argv[2];
				ProcessFile(sourcePath, destPath);
			}
			else if (argc > 1) // User specified source file but not dest file.
			{
				fs::path filePath(sourcePath);
				filePath.replace_extension("cdat");
				destPath = filePath.generic_string();
				ProcessFile(sourcePath, destPath);
			}
		}
		else // User didn't specify anything.
		{
			std::vector<std::string> generatedFiles;
			for (const auto& p : std::filesystem::recursive_directory_iterator("assets/geometry/")) {
				const fs::path& path = p.path();
				if (path.extension() == ".fbx")
				{
					std::string destFile;
					std::string pathStr = path.generic_string();
					std::transform(pathStr.begin(), pathStr.end(), pathStr.begin(),
						[](const unsigned char c) { return std::tolower(c); });
					if (pathStr.find("high") != std::string::npos)
					{
						destFile = path.parent_path().append("lod0.cdat").generic_string();
					}
					else if (pathStr.find("lod0") != std::string::npos)
					{
						destFile = path.parent_path().append("lod1.cdat").generic_string();
					}
					else if (pathStr.find("lod1") != std::string::npos)
					{
						destFile = path.parent_path().append("lod2.cdat").generic_string();
					}
					else if (pathStr.find("lod2") != std::string::npos)
					{
						destFile = path.parent_path().append("lod3.cdat").generic_string();
					}
					else if (pathStr.find("lod3") != std::string::npos)
					{
						destFile = path.parent_path().append("lod4.cdat").generic_string();
					}
					else if (pathStr.find("lod4") != std::string::npos)
					{
						destFile = path.parent_path().append("lod5.cdat").generic_string();
					}
					else if (pathStr.find("lod5") != std::string::npos)
					{
						destFile = path.parent_path().append("lod6.cdat").generic_string();
					}
					else if (pathStr.find("lod6") != std::string::npos)
					{
						destFile = path.parent_path().append("lod7.cdat").generic_string();
					}
					else
					{
						fs::path filePath{ path };
						filePath.replace_extension("cdat");
						destFile = filePath.generic_string();
					}

					if (!destFile.empty())
					{
						ProcessFile(path.string(), destFile);
						generatedFiles.emplace_back(destFile);
					}
				}
			}
			std::cout << "Generated files:" << std::endl;
			for (const auto& generatedFile : generatedFiles)
			{
				std::cout << generatedFile << std::endl;
			}
		}
	}
	catch (const std::runtime_error&)
	{
		LOG_ERR("Fatal error");
	}

	return EXIT_SUCCESS;
}
