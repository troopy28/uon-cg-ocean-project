#pragma once

#include <string>
#include <thread>
#include "glm/fwd.hpp"

#define FILENAMEWITHOUTPATH (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

#define LOG_AND_THROW(message)	do { std::cout << std::this_thread::get_id() << "  | [error] " << FILENAMEWITHOUTPATH << " > " << __func__ << "[" <<  std::to_string(__LINE__) << "] " <<  (message) << std::endl; \
								throw std::runtime_error(std::string(message)); } while(false)

#define LOG_WARN(message)  std::cout << std::this_thread::get_id() << "  | [warning] " << FILENAMEWITHOUTPATH << " > " << __func__ << "[" <<  std::to_string(__LINE__) << "] " <<  (message) << std::endl

#define LOG_ERR(message)   std::cout << std::this_thread::get_id() << "  | [error] " << FILENAMEWITHOUTPATH << " > " << __func__ << "[" <<  std::to_string(__LINE__) << "] " <<  (message) << std::endl

#define LOG_DEBUG(x) LogDebug((x), FILENAMEWITHOUTPATH, __func__, std::to_string(__LINE__))
void LogDebug(const std::string & message, const std::string & filename, const std::string & function, const std::string & line);

std::string StringFromVector(const glm::vec3 & vec);

std::string StringFromMatrix4(const glm::mat4& mat);