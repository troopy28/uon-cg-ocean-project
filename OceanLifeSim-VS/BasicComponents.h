#pragma once

#include "Common.h"
#include <string>
#include <vector>
#include "AssetLoader.h"

using Color = glm::vec3;

namespace Colors
{
	const inline Color WHITE = { 1.0f, 1.0f, 1.0f };
	const inline Color BLACK = { 0.0f, 0.0f, 0.0f };
	const inline Color RED = { 1.0f, 0.0f, 0.0f };
	const inline Color GREEN = { 0.0f, 1.0f, 0.0f };
	const inline Color BLUE = { 0.0f, 0.0f, 1.0f };
}

/**
 * \brief Objects in the scene that need a transform should have
 * this component associated with them.
 */
struct CTransform
{
	glm::vec3 position{ 0.0f, 0.0f, 0.0f };
	/**
	 * \brief Use euler angles for rotations to simplify specifying the values etc.
	 */
	glm::vec3 rotation{ 0.0f, 0.0f, 0.0f };
	glm::vec3 scale{ 1.0f, 1.0f , 1.0f };

	[[nodiscard]] glm::mat<4, 4, float, glm::packed_highp> ComputeRotationMatrix() const
	{
		return glm::eulerAngleXYZ(glm::radians(rotation.x), glm::radians(rotation.y), glm::radians(rotation.z));
	}
};

struct CPlayer
{
	glm::vec3 front;
	glm::vec3 up;
	glm::vec3 right;
	float yaw;
	float pitch;
};

/******************************************************************/
/*                     3D GEOMETRY COMPONENTS                     */
/******************************************************************/

struct CMesh
{
	MeshDataHandle meshData;
};

struct LODLevel
{
	std::string path{};
	float sqrDistance{ -1.0f };
};

struct LODData
{
	/**
	 * \brief Path to the mesh for each level of details and distance at which they need to get loaded.
	 * ORDERED ARRAY. LOD0 (most detailed) IS AT INDEX 0.
	 */
	std::vector<LODLevel> levels{};
	/**
	 * \brief Level that is currently loaded.
	 */
	int currentLevel;
};

struct CLevelsOfDetails
{
	LODData* lodData;
};

struct CTerrainTile
{
	int chunkPosX;
	int chunkPosZ;
};

/**
 * \brief Stores one tile of the ocean surface. The different tiles are all identical in terms of
 * surface, but have different positions in the scene. As such, all this component stores is a
 * pointer
 */
struct COceanSurfaceTile
{
	int chunkPosX;
	int chunkPosZ;
};


/******************************************************************/
/*                      RENDERING COMPONENTS                      */
/******************************************************************/

struct CDirectionalLight
{
	Color color{};
	float intensity{ 1.0f };
};

struct CSimpleColorMaterial
{
	Color color;
};

struct CSimpleMaterial
{
	CSimpleMaterial(const Texture* diffuse, const Texture* normal, const Texture* specular) :
		diffuseMap(diffuse),
		normalMap(normal),
		specularMap(specular)
	{
	}

	const Texture* diffuseMap;
	const Texture* normalMap;
	const Texture* specularMap;
};

struct CPbrMaterial
{
	Texture diffuseMap{};
	Texture normalMap{};
	Texture roughnessMap{};
};


struct CCamera
{
	/**
	 * \brief Field of view in radians.
	 */
	float fov{0.0f};
	glm::mat4 viewMatrix{};
};

/******************************************************************/
/*                        AUDIO COMPONENTS                        */
/******************************************************************/

struct CAudioSource
{
	FMOD::Studio::EventDescription* eventDescription;
	FMOD::Studio::EventInstance* eventInstance;
};