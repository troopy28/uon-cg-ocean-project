#include "WorldGenerationSystem.h"
#include "Scene.h"
#include <set>

namespace
{
	constexpr float CHUNK_WORLD_SIZE = 16.0f;
	constexpr float CHUNK_WORLD_SIZE_INV = 1.0f / CHUNK_WORLD_SIZE;
	constexpr int CHUNK_GEO_RESOLUTION = 64;
	constexpr float CHUNK_GEO_RESOLUTION_INV = 1.0f / static_cast<float>(CHUNK_GEO_RESOLUTION);
	constexpr int CHUNK_VIEWING_RADIUS = 2;
}

WorldGenerationSystem::WorldGenerationSystem(Scene& scene, entt::registry& registry) :
	GameSystem("World Generation System", scene, registry),
	m_perlinNoise(123456, 12, 0.5),
	scatterNoise(44612, 1, 0.7)
{
}

WorldGenerationSystem::~WorldGenerationSystem() = default;

void WorldGenerationSystem::UpdateImpl()
{
	// 1. Get the position of the player on the chunk grid.
	const glm::vec3 playerPos = m_scene.GetPlayerPosition();
	const int playerChunkX = static_cast<int>(playerPos.x * CHUNK_WORLD_SIZE_INV);
	const int playerChunkZ = static_cast<int>(playerPos.z * CHUNK_WORLD_SIZE_INV);

	// 2. Setup the list of the chunks that should be loaded.
	std::set<std::pair<int, int>> requiredChunks;
	for (int i = -CHUNK_VIEWING_RADIUS - 1; i <= CHUNK_VIEWING_RADIUS; i++) // Add a chunk in the negative coordinates to make the view more symmetrical.
	{
		for (int j = -CHUNK_VIEWING_RADIUS - 1; j <= CHUNK_VIEWING_RADIUS; j++) // Same here.
		{
			requiredChunks.insert({ i + playerChunkX, j + playerChunkZ });
		}
	}

	// 3. Get the currently loaded chunks (all entities with a CTerrainTile component).
	const auto terrainTileEntities = m_registry.view<const CTransform, CTerrainTile, CMesh>();

	// 4. Remove the chunks already loaded from the required list, and add the ones that require unloading in a list.
	std::vector<entt::entity> chunksToUnload;
	terrainTileEntities.each([&requiredChunks, &chunksToUnload](const entt::entity entity, const CTransform&, CTerrainTile& tile, CMesh&)
		{
			const std::pair<int, int> pos{ tile.chunkPosX, tile.chunkPosZ };
			const auto& it = requiredChunks.find(pos);
			if (it != requiredChunks.end())
			{
				requiredChunks.erase(pos);
			}
			else
			{
				chunksToUnload.push_back(entity);
			}
		});

	// At this point, we know which chunks to unload, and which chunks to generate.

	// 5. Unload the chunks we do not want. This amounts to destroying the corresponding entities.
	for (const entt::entity entity : chunksToUnload)
	{
		const CTerrainTile& ctt = m_registry.get<CTerrainTile>(entity);
		const float chunkPosWorldX = static_cast<float>(ctt.chunkPosX) * CHUNK_WORLD_SIZE;
		const float chunkPosWorldZ = static_cast<float>(ctt.chunkPosZ) * CHUNK_WORLD_SIZE;
		// LOG_DEBUG("Unloading chunk at " + std::to_string(chunkPosWorldX) + ", " + std::to_string(chunkPosWorldZ));
		m_registry.destroy(entity);
	}

	// 6. Load the chunks we want and that are not here yet.
	for (const auto& pos : requiredChunks)
	{
		const float chunkPosWorldX = static_cast<float>(pos.first) * CHUNK_WORLD_SIZE;
		const float chunkPosWorldZ = static_cast<float>(pos.second) * CHUNK_WORLD_SIZE;
		const entt::entity chunkEntity = m_registry.create();
		m_registry.emplace<CTransform>(chunkEntity, glm::vec3(0.0, 0.0f, 0.0));
		m_registry.emplace<CTerrainTile>(chunkEntity, pos.first, pos.second);
		m_registry.emplace<CSimpleMaterial>(chunkEntity,
			&AssetLoader::GetTexture(ASSET_TEXTURE_SAND_DIFFUSE),
			&AssetLoader::GetTexture(ASSET_TEXTURE_SAND_NORMALS),
			&AssetLoader::GetTexture(ASSET_TEXTURE_SAND_ROUGHNESS));

		// Generate the chunk geometry itself.
		MeshDataHandle meshDataHandle = AssetLoader::FindOrAllocateMesh(
			"terrain_" + std::to_string(chunkPosWorldX) + "_" + std::to_string(chunkPosWorldZ));
		GenerateChunk(chunkPosWorldX, chunkPosWorldZ, meshDataHandle);
		GenerateScatter(chunkPosWorldX, chunkPosWorldZ);
		m_registry.emplace<CMesh>(chunkEntity, meshDataHandle);
		// LOG_DEBUG("Loaded chunk at " + std::to_string(chunkPosWorldX) + ", " + std::to_string(chunkPosWorldZ));
	}
}

void WorldGenerationSystem::GenerateChunk(const float chunkPosWorldX, const float chunkPosWorldZ, MeshDataHandle& meshDataHandle) const
{
	// Generate the vertices. Generation is done directly in world space.
	for (int i = 0; i < CHUNK_GEO_RESOLUTION; i++)
	{
		for (int j = 0; j < CHUNK_GEO_RESOLUTION; j++)
		{
			const float fi = static_cast<float>(i);
			const float fj = static_cast<float>(j);
			const float x = chunkPosWorldX + fi * CHUNK_WORLD_SIZE / (CHUNK_GEO_RESOLUTION - 1);
			const float z = chunkPosWorldZ + fj * CHUNK_WORLD_SIZE / (CHUNK_GEO_RESOLUTION - 1);
			float c = static_cast<float>(m_perlinNoise.perlinNoise2D(4, x, z));
			c = (c + 1.0f) * 0.5f;
			// vertex
			meshDataHandle->vertices.emplace_back(Vertex{
				glm::vec3(x, c * 10.0f, z),
				glm::vec3(0.0f, 1.0f, 0.0f), // TODO compute correct normal here.
				glm::vec3(1.0f, 0.0f, 0.0f), // TODO compute correct tangent here.
				glm::vec3(0.0f, 0.0f, 1.0f), // TODO compute correct bitangent here.
				glm::vec2(static_cast<float>(i) * CHUNK_GEO_RESOLUTION_INV, static_cast<float>(j) * CHUNK_GEO_RESOLUTION_INV)
				});
		}
	}

	// Generate the indices. Basic up-right bottom-left triangles indexation scheme.
	for (unsigned int j = 0; j < CHUNK_GEO_RESOLUTION - 1; j++)
	{
		for (unsigned int i = 0; i < CHUNK_GEO_RESOLUTION - 1; i++)
		{
			const unsigned int index = j * CHUNK_GEO_RESOLUTION + i;
			// Top right triangle.
			meshDataHandle->indices.push_back(index);
			meshDataHandle->indices.push_back(index + 1);
			meshDataHandle->indices.push_back(index + 1 + CHUNK_GEO_RESOLUTION);

			// Bottom left triangle.
			meshDataHandle->indices.push_back(index);
			meshDataHandle->indices.push_back(index + 1 + CHUNK_GEO_RESOLUTION);
			meshDataHandle->indices.push_back(index + CHUNK_GEO_RESOLUTION);
		}
	}
}

void WorldGenerationSystem::GenerateScatter(const float chunkPosWorldX, const float chunkPosWorldZ) const
{

	// Object Placement - RIAN

	std::vector<glm::vec2> coralScatter2D;
	std::vector<glm::vec2> ruinsScatter2D;

	for (int i = 0; i < CHUNK_GEO_RESOLUTION; i++)
	{
		for (int j = 0; j < CHUNK_GEO_RESOLUTION; j++)
		{
			const float fi = static_cast<float>(i);
			const float fj = static_cast<float>(j);
			const float myX = chunkPosWorldX + fi * CHUNK_WORLD_SIZE / (CHUNK_GEO_RESOLUTION - 1);
			const float myZ = chunkPosWorldZ + fj * CHUNK_WORLD_SIZE / (CHUNK_GEO_RESOLUTION - 1);


			float coralPoints = static_cast<float>(scatterNoise.perlinNoise2D(4, myX, myZ));
			coralPoints = (coralPoints + 1.0f) * 0.5f;

			if (coralPoints * 10 >= 7.5f) //threshold 
			{
				coralScatter2D.emplace_back(glm::vec2(myX, myZ));
				int vertexSize = coralScatter2D.size();

				if (vertexSize > 1) //loop skips the first index 
				{
					//if a location is too close to the previous location, it will erase the location from the vector

					if (coralScatter2D[vertexSize - 1][0] <= (coralScatter2D[vertexSize - 2][0] + 2) && // checks if the previous location is at least 2 units away
						coralScatter2D[vertexSize - 1][1] <= (coralScatter2D[vertexSize - 2][1]) + 2)
					{
						coralScatter2D.erase(coralScatter2D.begin() + vertexSize - 1, coralScatter2D.begin() + vertexSize);
					}
				}
			}



			float ruinsPoints = static_cast<float>(scatterNoise.perlinNoise2D(8, myX * 0.8, myZ * 0.8)); // * 0.8 reduces chance of overlap at chunk border
			ruinsPoints = (ruinsPoints + 1.0f) * 0.5f;

			if (ruinsPoints * 10 >= 7.2f) //threshold 
			{
				ruinsScatter2D.emplace_back(glm::vec2(myX, myZ));
				int vertexSize = ruinsScatter2D.size();

				if (vertexSize > 1)
				{
					if (ruinsScatter2D[vertexSize - 1][0] <= (ruinsScatter2D[vertexSize - 2][0] + 8) &&
						ruinsScatter2D[vertexSize - 1][1] <= (ruinsScatter2D[vertexSize - 2][1]) + 8)
					{
						ruinsScatter2D.erase(ruinsScatter2D.begin() + vertexSize - 1, ruinsScatter2D.begin() + vertexSize);
					}
				}
			}
		}
	}



	unsigned int coralCount = coralScatter2D.size();
	unsigned int ruinsCount = ruinsScatter2D.size();

	glm::vec3* coralPos = new glm::vec3[coralCount];
	glm::vec3* coralRot = new glm::vec3[coralCount];

	glm::vec3* ruinsPos = new glm::vec3[ruinsCount];
	glm::vec3* ruinsRot = new glm::vec3[ruinsCount];

	int xRotation;
	int yRotation;
	int zRotation;


	//CORAL SCATTERING ADJUSTMENTS
	if (coralCount > 0)
	{
		for (int k = 0; k <= coralCount - 1; k++)
		{
			float oceanFloorVertices = static_cast<float>(m_perlinNoise.perlinNoise2D(6, coralScatter2D[k][0], coralScatter2D[k][1]));
			oceanFloorVertices = ((oceanFloorVertices + 1.0f) * 0.5f) * 10.0f;

			float x = coralScatter2D[k][0];
			float y = oceanFloorVertices + 0.1f; // +0.1f; so the model sits on TOP of the ground and not IN the ground
			float z = coralScatter2D[k][1];

			yRotation = (rand() % 360);

			coralRot[k] = glm::vec3(0, yRotation, 0);

			coralPos[k] = glm::vec3(x, y, z);


			const entt::entity stoneEntity = m_registry.create();
			m_registry.emplace<CTransform>(stoneEntity, glm::vec3(coralPos[k]), coralRot[k], glm::vec3(0.03f));
			m_registry.emplace<CMesh>(stoneEntity, AssetLoader::FindOrLoadMeshFromFile(ASSET_MESH_MOSSY_ROCK2_LOD4));
			m_registry.emplace<CLevelsOfDetails>(stoneEntity, new LODData{
			{
				LODLevel{ASSET_MESH_MOSSY_ROCK2_LOD1, 4.0f * 4.0f},
				LODLevel{ASSET_MESH_MOSSY_ROCK2_LOD2, 8.0f * 8.0f},
				LODLevel{ASSET_MESH_MOSSY_ROCK2_LOD3, 10.0f * 10.0f},
				LODLevel{ASSET_MESH_MOSSY_ROCK2_LOD4, 12.0f * 12.0f},
			},3 });
			m_registry.emplace<CSimpleMaterial>(stoneEntity,
				&AssetLoader::GetTexture(ASSET_MESH_MOSSY_ROCK2_DIFFUSE),
				&AssetLoader::GetTexture(ASSET_MESH_MOSSY_ROCK2_NORMALS),
				&AssetLoader::GetTexture(ASSET_MESH_MOSSY_ROCK2_ROUGHNESS));
		}
	}


	//RUINS SCATTERING ADJUSTMENTS
	if (ruinsCount > 0)
	{
		for (int k = 0; k <= ruinsCount - 1; k++)
		{
			float oceanFloorVertices = static_cast<float>(m_perlinNoise.perlinNoise2D(4, ruinsScatter2D[k][0], ruinsScatter2D[k][1]));
			oceanFloorVertices = ((oceanFloorVertices + 1.0f) * 0.5f) * 10.0f;

			float heightAdjust = (rand() % 10 + (-10)) * 0.1; //rand float between -1.0 to -0.1

			float x = ruinsScatter2D[k][0];
			float y = oceanFloorVertices + heightAdjust;
			float z = ruinsScatter2D[k][1];

			xRotation = (rand() % 41 + (-20)); //random angle between -20 and 20 degrees
			yRotation = (rand() % 360);
			zRotation = (rand() % 41 + (-20));

			ruinsRot[k] = glm::vec3(xRotation, yRotation, zRotation);
			ruinsPos[k] = glm::vec3(x, y, z);


			const entt::entity pillarEntity = m_registry.create();
			m_registry.emplace<CTransform>(pillarEntity, glm::vec3(ruinsPos[k]), ruinsRot[k], glm::vec3(0.03f));
			m_registry.emplace<CMesh>(pillarEntity, AssetLoader::FindOrLoadMeshFromFile(ASSET_MESH_PILLAR1_LOD4));
			m_registry.emplace<CSimpleMaterial>(pillarEntity,
				&AssetLoader::GetTexture(ASSET_MESH_PILLAR1_DIFFUSE),
				&AssetLoader::GetTexture(ASSET_MESH_PILLAR1_NORMALS),
				&AssetLoader::GetTexture(ASSET_MESH_PILLAR1_ROUGHNESS));
		}
	}


}
