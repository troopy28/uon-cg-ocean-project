#include <glm/glm.hpp>

struct Position {
	glm::vec4 pos;
};

struct Material {
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
	glm::vec3 emissive;
	float shininess;
};

struct LightColorParameter {
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
};

struct SpotLightParameter {
	glm::vec4 direction;

	float cutoffAngle;
	float exponent;
};