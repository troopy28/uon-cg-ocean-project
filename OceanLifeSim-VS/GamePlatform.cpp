#include "GamePlatform.h"

#include <cstdlib>
#include <stdexcept>

#if _WIN32
#include <Windows.h>
#endif

namespace Time
{
	volatile float deltaTime{ 0.0f };
	volatile float time{ 0.0f };
	volatile unsigned long frame{ 0 };
}

namespace Core
{

	namespace
	{
		[[noreturn]] void GlfwErrorCallback(const int error, const char* description)
		{
			LOG_AND_THROW(std::string("[GLFW ERROR] ") + std::to_string(error) + " " + description);
		}
	}


	GamePlatform::GamePlatform() :
		m_window{ nullptr },
		m_inputManager{},
		m_bufferWidth{ 0 },
		m_bufferHeight{ 0 }
	{
		//Initialize GLFW  
		if (!glfwInit())
		{
			std::quick_exit(EXIT_FAILURE);
		}

		// Request at least OpenGL 3.
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		//Create a window and create its OpenGL context
		m_bufferWidth = 1280;
		m_bufferHeight = 720;
		m_window = glfwCreateWindow(m_bufferWidth, m_bufferHeight, "Test Window", nullptr, nullptr);
		

		//If the window couldn't be created  
		if (!m_window)
		{
			glfwTerminate();
			throw std::runtime_error("Failed to open GLFW window.");
		}

		// Store a pointer to this instance in the window's user pointer data.
		// This is because we can't use member pointers as callbacks for GLFW in the calls below, so this user pointer
		// is used as a way to access the "this" pointer.
		glfwSetWindowUserPointer(m_window, this);

		// Setup the different callbacks.
		glfwSetErrorCallback(GlfwErrorCallback);
		glfwSetFramebufferSizeCallback(m_window, [](GLFWwindow* window, const int width, const int height)
			{
				static_cast<GamePlatform*>(glfwGetWindowUserPointer(window))->OnFrameBufferResized(width, height);
			});
		glfwSetKeyCallback(m_window, [](GLFWwindow* window, const int key, const int scanCode, const int action, const int mods)
			{
				static_cast<GamePlatform*>(glfwGetWindowUserPointer(window))->m_inputManager.OnKeyChanged(key, scanCode, action, mods);
			});
		glfwSetCursorPosCallback(m_window, [](GLFWwindow* window, const double xPos, const double yPos)
			{
				static_cast<GamePlatform*>(glfwGetWindowUserPointer(window))->m_inputManager.OnCursorMoved(xPos, yPos);
			});

		//This function makes the context of the specified window current on the calling thread.   
		glfwMakeContextCurrent(m_window);
		// Capture the mouse but disable the pointer.
		glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

#ifdef _WIN32
		// Turn on vertical screen sync under Windows.
		// (I.e. it uses the WGL_EXT_swap_control extension)
		typedef BOOL(WINAPI* PFNWGLSWAPINTERVALEXTPROC)(int interval);
		PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT = NULL;
		wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");
		if (wglSwapIntervalEXT)
			wglSwapIntervalEXT(1);
#endif

		glewExperimental = GL_TRUE;

		//If GLEW hasn't initialized  .
		if (const GLenum err = glewInit(); err != GLEW_OK)
		{
			LOG_AND_THROW(reinterpret_cast<const char*>(glewGetErrorString(err)));
		}
	}

	void GamePlatform::RunMainLoop(std::function<void()>&& callback)
	{
		const Clock clock;
		float currentTime = clock.GetElapsedSeconds();
		do
		{
			const float newTime = clock.GetElapsedSeconds();
			Time::deltaTime = newTime - currentTime;
			currentTime = newTime;
			// 1. Manage the different events that have been received since the last
			// call to this function. This will register the pressed keys in the
			// input manager, as well as the mouse movements.
			glfwPollEvents();

			// 2. Call game systems. This is the core part of the loop. Calls the game
			// logic as well as the rendering systems. See in the main file.
			callback();
			Time::time += Time::deltaTime;

			// 3. Clear up the pressed and released keys. Down keys remain down until
			// they get released.
			m_inputManager.Clear();

			// 4. Swap the OpenGL buffers so that the stuff rendered in the callback
			// call are displayed on the screen.
			glfwSwapBuffers(m_window);
			Time::frame++;
		} while (!glfwWindowShouldClose(m_window));
	}

	void GamePlatform::ExitGame() const
	{
		glfwDestroyWindow(m_window);
		glfwTerminate();
	}

	GLFWwindow* GamePlatform::GetWindow() const
	{
		return m_window;
	}


	float GamePlatform::GetAspectRatio() const
	{
		return GetWidth<float>() / GetHeight<float>();
	}

	void GamePlatform::OnFrameBufferResized(const int width, const int height)
	{
		LOG_DEBUG("Framebuffer resized!");
		m_bufferWidth = width;
		m_bufferHeight = height;
		glViewport(0, 0, width, height);
	}
}
