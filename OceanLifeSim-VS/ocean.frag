#version 330 core
out vec4 FragColor;

in vec3 FragPos;
in vec3 Normal;

uniform vec3 viewPos;
uniform vec3 lightColor;
uniform vec3 lightPos;


void main()
{   
    vec3 oceanColor = vec3(0.4, 0.7, 1.0);
    vec3 ambient = 0.005 * oceanColor;
    
    // Vectors
    vec3 V = normalize(viewPos - FragPos);
    vec3 L = normalize(lightPos - FragPos);

    // diffuse
    float diff = max(dot(L, Normal), 0.0);
    vec3 diffuse = 0.2 *  diff * oceanColor;

    // specular
    vec3 reflectDir = reflect(-L, Normal);
    float spec = 0.0;
    vec3 halfwayDir = normalize(L + V);  
    spec = pow(max(dot(Normal, halfwayDir), 0.0), 32.0);
    vec3 specular = vec3(0.8) * spec;

    vec3 result = ambient + diffuse + specular;

    // Fog
    float distance = distance(FragPos, viewPos);
    vec3 background_color = vec3(0.29, 0.38, 0.62) * 0.8;
    result = mix(result, background_color, min(0.05 * distance, 1.0));

    // TESTS -----------
    float d = dot(Normal, V);
    vec3 dotVisualizer = vec3(d, d, d);

    FragColor = vec4(result, 1.0);
}