#include "PerlinNoise.h"
#include <cmath>
#include <iostream>


//constructor class - assigns seed, zoom, persistence
PerlinNoise::PerlinNoise(const int seed, const double zoom, const double persistence)
{
	this->seed = seed;
	this->zoom = zoom;
	this->persistence = persistence;
}


//sums the noise octaves and retuns a final noise value for x and y
double PerlinNoise::perlinNoise2D(const int octaves, const double x, const double y) const
{
	double returnVal = 0.0, maxH = 0.0;

	for (int i = 0; i < octaves; i++)
	{
		const double frequency = pow(2., i);
		const double amplitude = pow(persistence, i);
		maxH += amplitude;
		returnVal += (noise2D(x * frequency / zoom, y * frequency / zoom)) * amplitude;
	}

	return (returnVal / maxH);
}


//calcs noise values at each point of the unit square around the x and y from perlinNoise2D
double PerlinNoise::noise2D(const double x, const double y) const
{
	const double floorX = floor(x);
	const double floorY = floor(y);

	const double s = findNoise2D(floorX, floorY);
	const double t = findNoise2D(floorX + 1, floorY);
	const double u = findNoise2D(floorX, floorY + 1);
	const double v = findNoise2D(floorX + 1, floorY + 1);

	const double int1 = interpolate(s, t, x - floorX);
	const double int2 = interpolate(u, v, x - floorX);

	return interpolate(int1, int2, y - floorY);
}



//gives a pseudo random number between -1 and 1 
double PerlinNoise::findNoise2D(const double x, const double y) const
{
	int n = static_cast<int>(x) + static_cast<int>(y) * seed;
	n = (n << 13) ^ n; // << meanings left shifting  13 bits
	const long n2 = 60493L * static_cast<long>(pow(n, 3));
	const long nn = (n2 + 19990303 * n + 1376312589) & 0x7fffffff;	//a & b is 1 when both a and b are 1, for each binary digit in a and b.

	return 1.0 - (static_cast<double>(nn) / 1073741824.0);
}



double PerlinNoise::interpolate(const double a, const double b, const double x) const
{
	//Cosine Interpolation Formulas:
	//double ft=(x * 3.14159265358985);
	//double f = (1.0 - cos(ft)) * 0.5;
	//Linear Interpolation Formula:
	const double f = x;
	//Cubic Interpolation Formula:
	//double f = 3 * pow(x, 2) - 2 * pow(x, 3);
	//Quintic Interpolation Formula:
	//const double f = 6 * pow(x, 5) - 15 * pow(x, 4) + 10 * pow(x, 3);

	return a * (1.0 - f) + b * f;
}
