#include "Scene.h"
#include "BasicComponents.h"

Scene::Scene(std::string sceneName) :
	m_sceneName{ std::move(sceneName) },
	m_registry{},
	m_systems{},
	m_playerEntity{ m_registry.create() }
{
	// Initialize the player entity.
	m_registry.emplace<CTransform>(m_playerEntity, glm::vec3{ 0.0f, 20.0f, 3.0f });
	m_registry.emplace<CCamera>(m_playerEntity, glm::radians(45.0f));
	m_registry.emplace<CPlayer>(m_playerEntity,
		glm::vec3(0.0f, 0.0f, -1.0f),
		glm::vec3(0.0f, 1.0f, 0.0f),
		glm::vec3(1.0f, 0.0f, 0.0f),
		0.0f,
		0.0f);
}

const entt::registry& Scene::GetRegistry() const
{
	return m_registry;
}

entt::registry& Scene::GetRegistry()
{
	return m_registry;
}

void Scene::Update()
{
	// Call the game systems.
	for (const auto& system : m_systems)
	{
		system->Update();
	}
}

entt::entity Scene::GetPlayerEntity() const
{
	return m_playerEntity;
}

glm::vec3 Scene::GetPlayerPosition() const
{
	return m_registry.get<CTransform>(m_playerEntity).position;
}

const CCamera& Scene::GetCamera() const
{
	return m_registry.get<CCamera>(m_playerEntity);
}

