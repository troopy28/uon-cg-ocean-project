#include "LODSystem.h"

#include "Scene.h"


namespace
{
	int GetLevelForDistance(const LODData& lodData, const float sqrDist)
	{
		// Simple linear search. If profiling shows this is a bottleneck,
		// binary search will be considered. It shouldn't be necessary.

		int chosenLevel = static_cast<int>(lodData.levels.size() - 1); // Start at the furthest level.
		int level = chosenLevel;
		while (level >= 0 && sqrDist < lodData.levels[level].sqrDistance)
		{
			chosenLevel = level;
			level--;
		}
		return chosenLevel;
	}
}

namespace Rendering
{
	struct DistanceEntry
	{
		float minDistance{ 0.0f };
		LODData* lodData{ nullptr };
		std::vector<entt::entity> entities{};
	};

	LODSystem::LODSystem(Scene& scene, entt::registry& registry) :
		GameSystem("LOD System", scene, registry),
		m_keepWorkerAlive(true),
		m_requestQueue(),
		m_resultsQueue(),
		m_waitingEntities(),
		m_loadingThread(&LODSystem::LoadingWorker, this)
	{
	}

	LODSystem::~LODSystem()
	{
		m_keepWorkerAlive = false;
		m_loadingThread.join();
	}

	void LODSystem::UpdateImpl()
	{
		const glm::vec3 cameraPosition = m_scene.GetPlayerPosition();

		const auto view = m_registry.view<const CTransform, const CLevelsOfDetails, CMesh>();
#if 0
		// 1. First there is the special case of entities with null meshes (not loaded yet).
		// Load their lowest level of detail right away, don't request them. We need them now.
		// todo put those in a separate list and avoid this if nullptr ugly thing.
		view.each([](const CTransform&, const CLevelsOfDetails& lod, CMesh& mesh)
			{
				if(mesh.meshData.Get() == nullptr)
				{
					lod.lodData->currentLevel = static_cast<int>(lod.lodData->levels.size() - 1);
					mesh.meshData = AssetLoader::FindOrLoadMeshFromFile(lod.lodData->levels[lod.lodData->currentLevel].path);
				}
			});
#endif
		// 2. Distance to each mesh data block.
		std::unordered_map<const Core::MeshData*, DistanceEntry> minDistanceToLods{};
		view.each([&minDistanceToLods, cameraPosition, this](const entt::entity& entity, const CTransform& transform, const CLevelsOfDetails& lod, const CMesh& mesh)
			{
				if(m_waitingEntities.find(entity) == m_waitingEntities.end())
				{
					const float distance = glm::distance2(cameraPosition, transform.position);
					const auto& it = minDistanceToLods.find(mesh.meshData.Get());
					if (it != minDistanceToLods.end() && distance < it->second.minDistance)
					{
						it->second.minDistance = distance;
						it->second.entities.push_back(entity);
					}
					else
					{
						minDistanceToLods[mesh.meshData.Get()] = DistanceEntry{ distance, lod.lodData, {entity} };
					}
				}
			});
		// LOG_DEBUG("Computed squared distances.");

		// 3. Calculate the LOD to load for each mesh component.
		for (auto& [meshDataPtr, distEntry] : minDistanceToLods)
		{
			const int level = GetLevelForDistance(*distEntry.lodData, distEntry.minDistance);
			if (level != distEntry.lodData->currentLevel)
			{
				const std::string lodPath = distEntry.lodData->levels[level].path;
				for (entt::entity entity : distEntry.entities)
					m_waitingEntities.insert(entity);
				m_requestQueue.Push(LodLoadingRequest{ Time::frame, lodPath, std::move(distEntry.entities), level });
				//LOG_DEBUG("Requested loading of mesh " + lodPath);
			}
		}

		// 4. Iterate over the meshes that were loaded from the disk.
		auto optMessage = m_resultsQueue.Pop();
		while (optMessage.has_value())
		{
			// Update the components' mesh data pointer.
			LodLoadingResult& result = optMessage.value();
			//LOG_DEBUG("Received mesh " + result.filePath 
			//	+ " loaded over " + std::to_string(Time::frame - result.requestFrame) + " frames.");
			for (const entt::entity entity : result.entities)
			{
				CMesh& meshComponent = m_registry.get<CMesh>(entity);
				// COPPPPPYYYYYYY (that crash took a while to find). COPY the data. We have only one
				// result and several entities. Moving works only for 1-1 relationships not 1-many.
				meshComponent.meshData = result.meshData; 

				const CLevelsOfDetails& lodComponent = m_registry.get<CLevelsOfDetails>(entity);
				lodComponent.lodData->currentLevel = result.level;

				m_waitingEntities.erase(entity);
			}

			// Get the next result.
			optMessage = m_resultsQueue.Pop();
		}
	}

	// Executed on the loading thread.
	void LODSystem::LoadingWorker()
	{
		while (m_keepWorkerAlive)
		{
			std::optional<LodLoadingRequest> optMessage = m_requestQueue.Pop();
			while (optMessage.has_value())
			{
				LodLoadingRequest& request = optMessage.value();
				MeshDataHandle meshDataHandle = AssetLoader::FindOrLoadMeshFromFile(request.filePath);
				m_resultsQueue.Push(LodLoadingResult{
					request.requestFrame,
					std::move(meshDataHandle),
					request.filePath,
					std::move(request.entities),
					request.level
					});
				optMessage = m_requestQueue.Pop();
			}
			std::this_thread::yield();
		}
	}
}
