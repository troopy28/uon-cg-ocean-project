#pragma once

#include "GameSystem.h"
#include <thread>
#include "MessageQueue.h"
#include "AssetLoader.h"
#include "BasicComponents.h"

namespace Rendering
{

	struct LodLoadingRequest
	{
		unsigned long requestFrame;
		std::string filePath{};
		std::vector<entt::entity> entities{};
		int level;
	};

	struct LodLoadingResult
	{
		unsigned long requestFrame;
		MeshDataHandle meshData;
		std::string filePath{}; // just for debugging / printing
		std::vector<entt::entity> entities{};
		int level;
	};


	/**
	 * \brief Level of details system. Responsible for loading / unloading
	 * LOD meshes on the fly.
	 */
	class LODSystem : public Core::GameSystem
	{
	public:
		explicit LODSystem(Scene& scene, entt::registry& registry);
		~LODSystem() override;
		DISABLE_COPY(LODSystem);
		DISABLE_MOVE(LODSystem);
		void UpdateImpl() override;

		/**
		 * \brief Executed on the loading thread.
		 */
		void LoadingWorker();

	private:
		bool m_keepWorkerAlive;
		/**
		 * \brief Used by main thread and LOD loading thread to communicate
		 * FROM the main thread TO the loading thread.
		 */
		MessageQueue<LodLoadingRequest> m_requestQueue;
		MessageQueue<LodLoadingResult> m_resultsQueue;
		std::unordered_set<entt::entity> m_waitingEntities;
		std::thread m_loadingThread;
	};
}