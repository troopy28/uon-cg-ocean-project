#include "PlayerControlsSystem.h"

#include "InputManager.h"
#include "Scene.h"

#define PLAYER_CONTROLLER_OUTPUT_DEBUG 0

PlayerControlsSystem::PlayerControlsSystem(Scene& scene, entt::registry& registry) :
	GameSystem("Player Controls System", scene, registry),
	m_playerEntity(m_scene.GetPlayerEntity())
{
}

PlayerControlsSystem::~PlayerControlsSystem() = default;

void PlayerControlsSystem::UpdateImpl()
{
	CPlayer& playerComponent = m_registry.get<CPlayer>(m_playerEntity);
	CTransform& transform = m_registry.get<CTransform>(m_playerEntity);
	HandleRotation(transform, playerComponent);
	HandleTranslation(transform, playerComponent);

	CCamera& camera = m_registry.get<CCamera>(m_playerEntity);
	camera.viewMatrix = glm::lookAt(transform.position, transform.position + playerComponent.front, playerComponent.up);

#if PLAYER_CONTROLLER_OUTPUT_DEBUG
	LOG_DEBUG("View Matrix:");
	std::cout << StringFromMatrix4(camera.viewMatrix);
#endif
}

void PlayerControlsSystem::HandleRotation(CTransform& transform, CPlayer& playerComponent) const
{
	constexpr float SENSITIVITY = 0.1f;

	if (Input::IsKeyDown(Core::KeyCode::E))
	{
		transform.rotation.y += 10.0f * Time::deltaTime;
	}
	if (Input::IsKeyDown(Core::KeyCode::Q))
	{
		transform.rotation.y -= 10.0f * Time::deltaTime;
	}

	float xoffset = Input::GetMouseDeltaX();
	float yoffset = Input::GetMouseDeltaY();
	xoffset *= SENSITIVITY;
	yoffset *= SENSITIVITY;

	playerComponent.yaw += xoffset;
	playerComponent.pitch += yoffset;

	// make sure that when pitch is out of bounds, screen doesn't get flipped

	if (playerComponent.pitch > 89.0f)
		playerComponent.pitch = 89.0f;
	if (playerComponent.pitch < -89.0f)
		playerComponent.pitch = -89.0f;
		
	// update Front, Right and Up Vectors using the updated Euler angles
	UpdateCameraVectors(playerComponent);

#if PLAYER_CONTROLLER_OUTPUT_DEBUG
	if(abs(Input::GetMouseDeltaX()) + abs(Input::GetMouseDeltaY()) > 0.1f)
	{
		LOG_DEBUG("Mouse delta = (" + std::to_string(Input::GetMouseDeltaX()) + ", " + std::to_string(Input::GetMouseDeltaY()) + ")");
		LOG_DEBUG("Player rotated. Current rotation: ("
			+ std::to_string(transform.rotation.x) + ", "
			+ std::to_string(transform.rotation.y) + ", "
			+ std::to_string(transform.rotation.z) + ")");
	}
#endif
}

void PlayerControlsSystem::HandleTranslation(CTransform& transform, const CPlayer& playerComponent) const
{
	// Move the player.
	constexpr float normalMovementSpeed = 3.0f;
	constexpr float sprintFactor = 3.0f;
	float movementSpeed = normalMovementSpeed;
	if (Input::IsKeyDown(Core::KeyCode::LeftCtrl))
		movementSpeed *= sprintFactor;
	if (Input::IsKeyDown(Core::KeyCode::LeftShift))
		movementSpeed /= sprintFactor;

	// Forward.
	if(Input::IsKeyDown(Core::KeyCode::W))
	{
		transform.position += playerComponent.front * Time::deltaTime * movementSpeed;
	}
	// Backward.
	if (Input::IsKeyDown(Core::KeyCode::S))
	{
		transform.position -= playerComponent.front * Time::deltaTime * movementSpeed;
	}

	// Left.
	if (Input::IsKeyDown(Core::KeyCode::A))
	{
		transform.position -= playerComponent.right * Time::deltaTime * movementSpeed;
	}
	// Right.
	if (Input::IsKeyDown(Core::KeyCode::D))
	{
		transform.position += playerComponent.right * Time::deltaTime * movementSpeed;
	}
#if PLAYER_CONTROLLER_OUTPUT_DEBUG
	LOG_DEBUG("---------------------------------------");
	LOG_DEBUG("Player position: " + StringFromVector(transform.position));
	LOG_DEBUG("Front vector: " + StringFromVector(playerComponent.front));
	LOG_DEBUG("Up vector: " + StringFromVector(playerComponent.up));
	LOG_DEBUG("Right vector: " + StringFromVector(playerComponent.right));
	LOG_DEBUG("---------------------------------------");
#endif
}

void PlayerControlsSystem::UpdateCameraVectors(CPlayer& playerComponent) const
{
	// calculate the new Front vector
	glm::vec3 front;
	front.x = cos(glm::radians(playerComponent.yaw)) * cos(glm::radians(playerComponent.pitch));
	front.y = sin(glm::radians(playerComponent.pitch));
	front.z = sin(glm::radians(playerComponent.yaw)) * cos(glm::radians(playerComponent.pitch));
	playerComponent.front = glm::normalize(front);
	// also re-calculate the Right and Up vector
	playerComponent.right = glm::normalize(glm::cross(playerComponent.front, WORLD_UP));  // normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
	playerComponent.up = glm::normalize(glm::cross(playerComponent.right, playerComponent.front));
}
