#pragma once

#include "BasicComponents.h"
#include "GameSystem.h"

namespace Audio
{
	class AudioSystem final : public Core::GameSystem
	{
	public:
		AudioSystem(Scene& scene, entt::registry& registry);
		~AudioSystem() override;
		DISABLE_COPY(AudioSystem);
		DISABLE_MOVE(AudioSystem);

		void UpdateImpl() override;

	private:
		entt::entity m_playerEntity;
		FMOD::Studio::System* m_fmodSystem;
		FMOD::Studio::Bank* m_masterBank;
		FMOD::Studio::Bank* m_masterStringsBank;
	};
}