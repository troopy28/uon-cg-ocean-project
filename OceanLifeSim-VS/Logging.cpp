#include "Logging.h"

#include <iostream>
#include <mutex>
#include <iomanip>
#include <sstream>
#include <glm/glm.hpp>

namespace
{
	std::mutex mtx{};
}

void LogDebug(const std::string& message, const std::string& filename, const std::string& function, const std::string& line)
{
	std::lock_guard<std::mutex> guard(mtx);
	std::cout << std::this_thread::get_id() << "  | [debug] " << filename << " > " << function << "[" << line << "] " << (message) << std::endl;
}

std::string StringFromVector(const glm::vec3& vec)
{
	std::stringstream stream;
	stream
		<< std::fixed << std::setprecision(2) << std::setw(5)
		<< "(" << vec.x << ", " << vec.y << ", " << vec.z << ")";
	return stream.str();
}

std::string StringFromMatrix4(const glm::mat4& mat)
{
	std::stringstream stream;
	stream
		<< std::fixed << std::setprecision(2) << std::setw(5)
		// [col][row]
		<< mat[0][0] << ", " << mat[1][0] << ", " << mat[2][0] << ", " << mat[3][0] << "\n"
		<< mat[0][1] << ", " << mat[1][1] << ", " << mat[2][1] << ", " << mat[3][1] << "\n"
		<< mat[0][2] << ", " << mat[1][2] << ", " << mat[2][2] << ", " << mat[3][2] << "\n"
		<< mat[0][3] << ", " << mat[1][3] << ", " << mat[2][3] << ", " << mat[3][3] << std::endl;
	return stream.str();
}