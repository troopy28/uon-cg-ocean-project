#pragma once

#ifndef PERLINNOISE_H
#define PERLINNOISE_H

class PerlinNoise
{
public:

	PerlinNoise(int seed, double zoom, double persistence);
	[[nodiscard]] double perlinNoise2D(int octaves, double x, double y) const;

	int seed;
	double zoom;
	double persistence;

private:
	[[nodiscard]] double noise2D(double x, double y) const;
	[[nodiscard]] double findNoise2D(double x, double y) const;
	[[nodiscard]] double interpolate(double a, double b, double x) const;
};

#endif