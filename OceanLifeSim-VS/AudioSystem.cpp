#include "AudioSystem.h"

#include "Scene.h"
#include <stdlib.h>

namespace Audio
{
	double randMToN(double M, double N)
	{
		return M + (rand() / (RAND_MAX / (N - M)));
	}

	AudioSystem::AudioSystem(Scene& scene, entt::registry& registry) :
		GameSystem("Audio System", scene, registry),
		m_playerEntity(m_scene.GetPlayerEntity()),
		m_fmodSystem(nullptr),
		m_masterBank(nullptr),
		m_masterStringsBank(nullptr)
	{
		srand(1234);

		FMOD_ERR_CHECK(FMOD::Studio::System::create(&m_fmodSystem));
		FMOD::System* coreSystem = nullptr;
		FMOD_ERR_CHECK(m_fmodSystem->getCoreSystem(&coreSystem));
		FMOD_ERR_CHECK(coreSystem->setSoftwareFormat(0, FMOD_SPEAKERMODE_STEREO, 0));

		FMOD_ERR_CHECK(m_fmodSystem->initialize(1024, FMOD_STUDIO_INIT_NORMAL, FMOD_INIT_NORMAL, nullptr));
		FMOD_ERR_CHECK(m_fmodSystem->loadBankFile(ASSET_AUDIO_MASTER_BANK_FILE_PATH, FMOD_STUDIO_LOAD_BANK_NORMAL, &m_masterBank));
		FMOD_ERR_CHECK(m_fmodSystem->loadBankFile(ASSET_AUDIO_MASTER_STRINGS_BANK_FILE_PATH, FMOD_STUDIO_LOAD_BANK_NORMAL, &m_masterStringsBank));

		constexpr float range = 50.0f;
		// Scatter audio sources
		for(int i = 0; i < 30; i++)
		{
			const auto entity = m_registry.create();
			CTransform& transform = m_registry.emplace<CTransform>(entity);
			transform.position = { randMToN(-range, range), randMToN(0.0, 20.0),randMToN(-range, range) };
			CAudioSource& audioSource = m_registry.emplace<CAudioSource>(entity);
			FMOD_ERR_CHECK(m_fmodSystem->getEvent("event:/Ambiance 2", &audioSource.eventDescription));
			FMOD_ERR_CHECK(audioSource.eventDescription->createInstance(&audioSource.eventInstance));
			FMOD_ERR_CHECK(audioSource.eventInstance->start());
		}

		for (int i = 0; i < 30; i++)
		{
			const auto entity = m_registry.create();
			CTransform& transform = m_registry.emplace<CTransform>(entity);
			transform.position = { randMToN(-range, range), randMToN(0.0, 20.0),randMToN(-range, range) };
			CAudioSource& audioSource = m_registry.emplace<CAudioSource>(entity);
			FMOD_ERR_CHECK(m_fmodSystem->getEvent("event:/Diving 2", &audioSource.eventDescription));
			FMOD_ERR_CHECK(audioSource.eventDescription->createInstance(&audioSource.eventInstance));
			FMOD_ERR_CHECK(audioSource.eventInstance->start());
		}
	}

	AudioSystem::~AudioSystem() = default;

	FMOD_VECTOR FmodVec(const glm::vec3& glmVec)
	{
		return { glmVec.x, glmVec.z, glmVec.y };
	}

	void AudioSystem::UpdateImpl()
	{
		// 1. Move the FMOD listener to the current position of the player in the scene.
		const CTransform& playerTransform = m_registry.get<CTransform>(m_playerEntity);
		const CPlayer& playerComponent = m_registry.get<CPlayer>(m_playerEntity);
		
		const FMOD_3D_ATTRIBUTES playerAttributes =
		{
			FmodVec(playerTransform.position),
			{}, FmodVec(playerComponent.front),
			FmodVec(playerComponent.up)
		};
		FMOD_ERR_CHECK(m_fmodSystem->setListenerAttributes(0, &playerAttributes));

		// 2. Update the position of the audio sources every other frame.
		constexpr unsigned long sourceRefreshRate = 5;
		if (Time::frame % sourceRefreshRate == 0)
		{
			const auto audioSourceViews = m_registry.view<const CTransform, const CAudioSource>();
			audioSourceViews.each([this](const CTransform& sourceTransform, const CAudioSource& audioSource)
				{
					const FMOD_3D_ATTRIBUTES sourceAttributes =
					{
						FmodVec(sourceTransform.position),
						FmodVec({ 0.0f, .0f, .0f }),
						FmodVec({1.0f, 0.0f, 0.0f}),
						FmodVec(LOCAL_UP)
					};
#if 0
					std::cout << "Source "
						<< sourceTransform.position.x << ", "
						<< sourceTransform.position.y << ", "
						<< sourceTransform.position.z << " | "
						<< "Listener "
						<< playerTransform.position.x << ", "
						<< playerTransform.position.y << ", "
						<< playerTransform.position.z << "; "
						<< playerTransform.rotation.x << ", "
						<< playerTransform.rotation.y << ", "
						<< playerTransform.rotation.z
						<< std::endl;
#endif
					FMOD_ERR_CHECK(audioSource.eventInstance->set3DAttributes(&sourceAttributes));
				});
		}

		// 3. Update the system.
		FMOD_ERR_CHECK(m_fmodSystem->update());
	}
}
