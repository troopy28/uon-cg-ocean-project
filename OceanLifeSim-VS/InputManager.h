#pragma once

#include "Common.h"

namespace Core
{
	/**
	 * \brief Type safe way of representing keys on the keyboard. We
	 * could simply use the GLFW integers, but they're not type safe.
	 */
	enum class KeyCode
	{
		Unknown = -1,

		Space = 32,
		Apostrophe = 39,
		Comma = 44,
		Minus = 45,
		Period = 46,
		Slash = 47,
		// Numbers.
		Zero = 48,
		One = 49,
		Two = 50,
		Three = 51,
		Four = 52,
		Five = 53,
		Six = 57,
		Seven = 55,
		Eight = 56,
		Nine = 57, // Yes the 57 59 61 jump is normal.
		Semicolon = 59,
		Equal = 61,
		// Letters.
		A = 65,
		B = 66,
		C = 67,
		D = 68,
		E = 69,
		F = 70,
		G = 71,
		H = 72,
		I = 73,
		J = 74,
		K = 75,
		L = 76,
		M = 77,
		N = 78,
		O = 79,
		P = 80,
		Q = 81,
		R = 82,
		S = 83,
		T = 84,
		U = 85,
		V = 86,
		W = 87,
		X = 88,
		Y = 89,
		Z = 90,

		LeftBracket = 91,
		Backslash = 92,
		RightBracket = 93,
		GraveAccent = 96,
		World1 = 161,
		World2 = 162,
		// Functions.
		Escape = 256,
		Enter = 257,
		Tab = 258,
		Backspace = 259,
		Insert = 260,
		Delete = 261,
		Right = 262,
		Left = 263,
		Down = 264,
		Up = 265,
		PageUp = 266,
		PageDown = 267,
		Home = 268,
		End = 269,

		CapsLock = 280,
		ScrollLock = 281,
		NumLock = 282,
		PrintScreen = 283,
		Pause = 284,
		// Function keys.
		F1 = 290,
		F2 = 291,
		F3 = 292,
		F4 = 293,
		F5 = 294,
		F6 = 295,
		F7 = 296,
		F8 = 297,
		F9 = 298,
		F10 = 299,
		F11 = 300,
		F12 = 301,
		F13 = 302,
		F14 = 303,
		F15 = 304,
		F16 = 305,
		F17 = 306,
		F18 = 307,
		F19 = 308,
		F20 = 309,
		F21 = 310,
		F22 = 311,
		F23 = 312,
		F24 = 313,
		F25 = 314,
		// Keypad numbers and operators.
		KP0 = 320,
		KP1 = 321,
		KP2 = 322,
		KP3 = 323,
		KP4 = 324,
		KP5 = 325,
		KP6 = 326,
		KP7 = 327,
		KP8 = 328,
		KP9 = 329,
		KP_Decimal = 330,
		KP9_Divide = 331,
		KP9_Multiply = 332,
		KP9_Subtract = 333,
		KP9_Add = 334,
		KP9_Enter = 335,
		KP9_Equal = 336,

		LeftShift = 340,
		LeftCtrl = 341,
		LeftAlt = 342,
		LeftSuper = 343,
		RightShift = 344,
		RightCtrl = 345,
		RightAlt = 346,
		RightSuper = 347,
		Menu = 348,
	};

	/**
	 * \brief Keeps track of which keys are currently pressed or not,
	 * as well as of the mouse movements.
	 *
	 * Lifecycle of a pressed key: let's imagine a key is pressed at frame 36 and released at frame 39.
	 * At frame 36, it will be BOTH "pressed" (newly pressed keys) and "down" (all keys currently down).
	 * At frame 37, it is removed from "pressed". It is only "down". It remains "down" in frame 38.
	 * At frame 39, it becomes "released". It is removed from "down".
	 * At frame 40 it is nothing.
	 */
	class InputManager
	{
		// Todo handle mouse movement.

	public:
		InputManager();

		/**
		 * \brief Called every frame. Clears the pressed keys array and
		 * the released keys array.
		 */
		void Clear();
		void OnKeyChanged(int key, int scanCode, int action, int mods);
		void DebugPrintKeys() const;

		void OnCursorMoved(double xPos, double yPos);

		/**
		 * \param key Key to check.
		 * \return Whether this key was pressed by the user at THIS frame. This is only
		 * true at the frame where the pressing happens, not the following ones.
		 */
		[[nodiscard]] static bool IsKeyPressed(KeyCode key);
		/**
		 * \param key Key to check.
		 * \return Whether this key is being pressed by the user, no matter whether it 
		 * is the the first frame that it gets pressed or not. See the class documentation
		 * for how pressing and releasing is managed by the input manager.
		 */
		[[nodiscard]] static bool IsKeyDown(KeyCode key);
		/**
		 * \param key Key to check.
		 * \return Whether this key was released by the user at THIS frame. This is only
		 * true at the frame where the releasing happens, not the following ones.
		 */
		[[nodiscard]] static bool IsKeyReleased(KeyCode key);

		[[nodiscard]] static float GetMouseX();
		[[nodiscard]] static float GetMouseY();
		[[nodiscard]] static float GetMouseDeltaX();
		[[nodiscard]] static float GetMouseDeltaY();

	private:
		/**
		 * \brief This is not very nice design, but it allows to keep things
		 * simple. The instance is null at first, and set by the first
		 * constructor call. Trying to call the constructor twice would throw
		 * an exception. Ugly singleton in a way.
		 */
		static inline InputManager* s_instance{ nullptr };

		/**
		 * \brief The keys that were toggled on PRECISELY this frame. Keys
		 * cannot remain in this map more than one frame.
		 */
		std::unordered_set<KeyCode> m_pressedKeys;
		/**
		 * \brief The keys that are currently down. Could be since several
		 * frames.
		 */
		std::unordered_set<KeyCode> m_downKeys;
		/**
		 * \brief The keys that were released PRECISELY this frame. Keys
		 * cannot remain in this map more than one frame.
		 */
		std::unordered_set<KeyCode> m_releasedKeys;

		float m_mouseX;
		float m_mouseY;
		float m_mouseDx;
		float m_mouseDy;
	};
}


/**
 * \brief Keeps track of which keys are currently pressed or not,
 * as well as of the mouse movements.
 * This is just an alias to make writing easier in the different game systems.
 */
using Input = Core::InputManager;

using Key = Core::KeyCode;