#pragma once

#include "Common.h"

struct Vertex
{
	glm::vec3 pos{ 0.0f };        // 0
	glm::vec3 normal{ 0.0f };     // 12
	glm::vec3 tangent{ 0.0f };    // 24
	glm::vec3 bitangent{ 0.0f };  // 36
	glm::vec2 uv{ 0.0f };		    // 48
	// Note: ensuring there's no padding is important to ensure data contiguity to pass the arrays
	// to OpenGL directly. Each vec3 is 12 bytes, and thus must start at a position in the struct
	// that is a multiple of 12. For vec2, it is 8 bytes.
};

namespace Core
{
	/**
	 * \brief Stores a triangle mesh. Vertices and indices.
	 */
	struct MeshData
	{
		MeshData();
		MeshData(std::vector<Vertex> _vertices, std::vector<uint32_t> _indices);
		DISABLE_COPY(MeshData);
		MeshData(MeshData&& other) noexcept;
		MeshData& operator=(MeshData&& other) noexcept;
		~MeshData();

		void SetupOpenGlData();

		/**
		 * \brief Individual vertices, no duplicates. Goes straight in OpenGL vertex buffers.
		 */
		std::vector<Vertex> vertices;
		/**
		 * \brief Indices to connect vertices into faces. Goes straight in OpenGL index buffers.
		 */
		std::vector<uint32_t> indices;
		/**
		 * \brief Just used for debugging purposes.
		 */
		std::string meshName{};
		/**
		 * \brief Vertex Array Object.
		 */
		uint32_t vao{};
		/**
		 * \brief Vertex Buffer Object.
		 */
		uint32_t vbo{};
		/**
		 * \brief Element Buffer Object.
		 */
		uint32_t ebo{};

		bool openglDataInitialized{ false };
	};

	using ReleaseAcquireFuncPtr = std::add_pointer_t<void(const Core::MeshData*, const std::string& reason)>;
}

class MeshDataHandle
{
public:
	MeshDataHandle(Core::MeshData* data, const Core::ReleaseAcquireFuncPtr notify, const Core::ReleaseAcquireFuncPtr release) :
		m_dataPtr(data),
		m_notifyUsingMesh(notify),
		m_notifyReleasingMesh(release)
	{
		m_notifyUsingMesh(m_dataPtr, "Creation");
	}

	MeshDataHandle(const MeshDataHandle& other) :
		m_dataPtr(other.m_dataPtr),
		m_notifyUsingMesh(other.m_notifyUsingMesh),
		m_notifyReleasingMesh(other.m_notifyReleasingMesh)
	{
		m_notifyUsingMesh(m_dataPtr, "Copy ctor");
	}

	MeshDataHandle& operator=(const MeshDataHandle& other)
	{
		if (&other == this)
			return *this;

		m_notifyReleasingMesh(m_dataPtr, "Copying another object (copy op), gotta release the current one");
		m_dataPtr = other.m_dataPtr;
		m_notifyUsingMesh = other.m_notifyUsingMesh;
		m_notifyReleasingMesh = other.m_notifyReleasingMesh;
		m_notifyUsingMesh(m_dataPtr, "Copy op");
		return *this;
	}

	MeshDataHandle(MeshDataHandle&& dying) noexcept :
		m_dataPtr(dying.m_dataPtr),
		m_notifyUsingMesh(dying.m_notifyUsingMesh),
		m_notifyReleasingMesh(dying.m_notifyReleasingMesh)
	{
		// We steal the data, the dying object won't have it anymore,
		// no need to signal data acquisition.
		dying.m_dataPtr = nullptr;
	}

	MeshDataHandle& operator=(MeshDataHandle&& dying) noexcept
	{
		// We steal the data, the dying object won't have it anymore,
		// no need to signal data acquisition.
		m_notifyReleasingMesh(m_dataPtr, "Stealing another object (moving op), gotta release the current one");
		m_notifyUsingMesh = dying.m_notifyUsingMesh;
		m_notifyReleasingMesh = dying.m_notifyReleasingMesh;
		m_dataPtr = dying.m_dataPtr;
		dying.m_dataPtr = nullptr;
		return *this;
	}

	~MeshDataHandle()
	{
		m_notifyReleasingMesh(m_dataPtr, "Destruction"); // Call even if data ptr is null, the asset loader takes care of it.
	}

	[[nodiscard]] const Core::MeshData* Get() const
	{
		return m_dataPtr;
	}

	[[nodiscard]] const Core::MeshData* operator->() const
	{
		return m_dataPtr;
	}

	[[nodiscard]] Core::MeshData* operator->()
	{
		return m_dataPtr;
	}

	[[nodiscard]] const Core::MeshData& operator*() const
	{
		return *m_dataPtr;
	}

	[[nodiscard]] Core::MeshData& operator*()
	{
		return *m_dataPtr;
	}

private:
	Core::MeshData* m_dataPtr;
	Core::ReleaseAcquireFuncPtr m_notifyUsingMesh;
	Core::ReleaseAcquireFuncPtr m_notifyReleasingMesh;
};

/**
 * \brief Contains a texture's pixel data, as well as the texture size and
 * the channels of the texture (GL_RGB or GL_RGBA) to pass to glTexImage2D.
 *
 * \note All the annoying constructors business is to ensure the object
 * can be added in the asset loader's texture cache (hash map) all the while
 * not being copyable.
 */
struct Texture
{
	Texture() :
		width(0),
		height(0),
		glChannel(0),
		glId(0),
		data(nullptr)
	{
	}

	Texture(const int w, const int h, const int glc, const unsigned char* texData) :
		width(w),
		height(h),
		glChannel(glc),
		glId(0),
		data(texData)
	{
		glGenTextures(1, &glId);
		if (data)
		{
			glBindTexture(GL_TEXTURE_2D, glId);
			glTexImage2D(GL_TEXTURE_2D, 0, glChannel, width, height, 0, glChannel, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}
		else
		{
			LOG_AND_THROW("Received nullptr for the texture data, cannot inialize OpenGL texture.");
		}
	}

	DISABLE_COPY(Texture);

	Texture(Texture&& other) noexcept :
		width(other.width),
		height(other.height),
		glChannel(other.width),
		glId(other.glId),
		data(other.data)
	{
		other.data = nullptr;
		other.glId = -1;
	}

	Texture& operator=(Texture&& other) noexcept
	{
		width = other.width;
		height = other.height;
		glChannel = other.width;
		glId = other.glId;
		data = other.data;
		other.data = nullptr;
		other.glId = -1;
		return *this;
	}

	~Texture()
	{
		delete data;
	}

	/**
	 * \brief Width of the texture in pixels.
	 */
	int width;
	/**
	 * \brief Height of the texture in pixels.
	 */
	int height;
	/**
	 * \brief GL_RGB or GL_RGBA. Should be passed to glTexImage2D.
	 */
	GLint glChannel;
	/**
	 * \brief OpenGL ID of the texture. This is passed to glBindTexture.
	 */
	GLuint glId;
	/**
	 * \brief Pixels of the texture, in the format specified by glChannel.
	 */
	const unsigned char* data;
};

#define ASSET_MESH_OCEAN_ANIMATION_FILE_PATH "assets/geometry/ocean/Ocean.mdd"
#define ASSET_MESH_OCEAN_REST_FRAME_FILE_PATH "assets/geometry/ocean/Ocean.cdat"

// Simple shader.
#define ASSET_SHADER_SIMPLE_FRAG  "assets/shaders/simpleshader.frag"
#define ASSET_SHADER_SIMPLE_VERTEX "assets/shaders/simpleshader.vert"

// Single-color shader.
#define ASSET_SHADER_SINGLE_COLOR_FRAG  "assets/shaders/singlecolor.frag"
#define ASSET_SHADER_SINGLE_COLOR_VERTEX "assets/shaders/singlecolor.vert"

// Shadow Map shader.
#define ASSET_SHADER_SHADOW_MAP_FRAG  "assets/shaders/shadowMapping.frag"
#define ASSET_SHADER_SHADOW_MAP_VERTEX "assets/shaders/shadowMapping.vert"

// Ocean shader.
#define ASSET_SHADER_OCEAN_FRAG  "assets/shaders/ocean.frag"
#define ASSET_SHADER_OCEAN_VERTEX "assets/shaders/ocean.vert"

// Skybox shader.
#define ASSET_SHADER_SKYBOX_FRAG  "assets/shaders/skybox.frag"
#define ASSET_SHADER_SKYBOX_VERTEX "assets/shaders/skybox.vert"

// Sand textures.
#define ASSET_TEXTURE_SAND_DIFFUSE "assets/textures/sand/tex_albedo.jpg"
#define ASSET_TEXTURE_SAND_NORMALS "assets/geometry/pillar1/tex_normals.jpg"
#define ASSET_TEXTURE_SAND_ROUGHNESS "assets/geometry/pillar1/tex_roughness.jpg"


// Pillar 1.
#define ASSET_MESH_PILLAR1_LOD0 "assets/geometry/pillar1/lod0.cdat"
#define ASSET_MESH_PILLAR1_LOD1 "assets/geometry/pillar1/lod1.cdat"
#define ASSET_MESH_PILLAR1_LOD2 "assets/geometry/pillar1/lod2.cdat"
#define ASSET_MESH_PILLAR1_LOD3 "assets/geometry/pillar1/lod3.cdat"
#define ASSET_MESH_PILLAR1_LOD4 "assets/geometry/pillar1/lod4.cdat"
#define ASSET_MESH_PILLAR1_LOD5 "assets/geometry/pillar1/lod5.cdat"
#define ASSET_MESH_PILLAR1_DIFFUSE "assets/geometry/pillar1/tex_albedo.jpg"
#define ASSET_MESH_PILLAR1_NORMALS "assets/geometry/pillar1/tex_normals.jpg"
#define ASSET_MESH_PILLAR1_ROUGHNESS "assets/geometry/pillar1/tex_roughness.jpg"

// Mossy rock 1.
#define ASSET_MESH_MOSSY_ROCK1_LOD0 "assets/geometry/mossy_rock1/lod0.cdat"
#define ASSET_MESH_MOSSY_ROCK1_LOD1 "assets/geometry/mossy_rock1/lod1.cdat"
#define ASSET_MESH_MOSSY_ROCK1_LOD2 "assets/geometry/mossy_rock1/lod2.cdat"
#define ASSET_MESH_MOSSY_ROCK1_LOD3 "assets/geometry/mossy_rock1/lod3.cdat"
#define ASSET_MESH_MOSSY_ROCK1_LOD4 "assets/geometry/mossy_rock1/lod4.cdat"
#define ASSET_MESH_MOSSY_ROCK1_DIFFUSE "assets/geometry/mossy_rock1/tex_albedo.jpg"
#define ASSET_MESH_MOSSY_ROCK1_NORMALS "assets/geometry/mossy_rock1/tex_normals.jpg"
#define ASSET_MESH_MOSSY_ROCK1_ROUGHNESS "assets/geometry/mossy_rock1/tex_roughness.jpg"

// Mossy rock 2. -- note that they're shifted for some forgotten reason (high res file lost somewhere)
#define ASSET_MESH_MOSSY_ROCK2_LOD0 "assets/geometry/mossy_rock2/lod1.cdat"
#define ASSET_MESH_MOSSY_ROCK2_LOD1 "assets/geometry/mossy_rock2/lod2.cdat"
#define ASSET_MESH_MOSSY_ROCK2_LOD2 "assets/geometry/mossy_rock2/lod3.cdat"
#define ASSET_MESH_MOSSY_ROCK2_LOD3 "assets/geometry/mossy_rock2/lod4.cdat"
#define ASSET_MESH_MOSSY_ROCK2_LOD4 "assets/geometry/mossy_rock2/lod5.cdat"
#define ASSET_MESH_MOSSY_ROCK2_DIFFUSE "assets/geometry/mossy_rock2/tex_albedo.jpg"
#define ASSET_MESH_MOSSY_ROCK2_NORMALS "assets/geometry/mossy_rock2/tex_normals.jpg"
#define ASSET_MESH_MOSSY_ROCK2_ROUGHNESS "assets/geometry/mossy_rock2/tex_roughness.jpg"


#define ASSET_AUDIO_MASTER_BANK_FILE_PATH "assets/audio/Master.bank"
#define ASSET_AUDIO_MASTER_STRINGS_BANK_FILE_PATH "assets/audio/Master.strings.bank"


namespace AssetLoader
{
	/**
	 * \brief Retrieves the content of the file at the given path, with a caching mechanism.
	 * A file cannot be read twice, which minimizes IO calls, and reading a file twice will
	 * return the same string, which reduces memory duplication/use.
	 *
	 * \param filePath Path of the text file to read.
	 * \return Constant reference to a string containing the file content.
	 */
	[[nodiscard]] const std::string& GetTextFile(const std::string& filePath);

	[[nodiscard]] const Texture& GetTexture(const std::string& filePath);

	[[nodiscard]] MeshDataHandle FindOrLoadMeshFromFile(const std::string& filePath);

	[[nodiscard]] MeshDataHandle FindOrAllocateMesh(const std::string& name);


}
