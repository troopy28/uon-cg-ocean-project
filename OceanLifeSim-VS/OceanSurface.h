#pragma once

#include "BasicComponents.h"
#include "GameSystem.h"


/**
 * \brief Stores the surface animation, and at each frame loads the next frame of the animation
 * to the different ocean tiles.
 */
class OceanSurfaceSystem final : public Core::GameSystem
{
public:
	OceanSurfaceSystem(Scene& scene, entt::registry& registry);
	~OceanSurfaceSystem() override;
	DISABLE_COPY(OceanSurfaceSystem);
	DISABLE_MOVE(OceanSurfaceSystem);

	void UpdateImpl() override;

private:
	int LoadOceanAnimation();

	void UpdateOceanSurface();
	void ManageTiles();

	std::vector<std::vector<glm::vec3>> m_animationFrames;
	size_t m_currentFrame;
	MeshDataHandle m_tileMeshHandle;
	int m_oceanChunkRes;
};
