#pragma once

#include "Common.h"
#include <thread>

class Scene;

namespace Core
{
	class GameSystem
	{
	public:
		/**
		 * \brief Stores the name of the system.
		 * \param systemName Name of the system.
		 * \param scene Scene the system is going to act on.
		 * \param registry Registry the system is going to act on.
		 */
		GameSystem(std::string systemName, Scene& scene, entt::registry& registry) :
			m_registry(registry),
			m_scene(scene),
			m_systemName(std::move(systemName)),
			m_timeBuffer(),
			m_runningAvgIdx(0)
		{
		}

		virtual ~GameSystem() = default;

		DISABLE_COPY(GameSystem);
		DISABLE_MOVE(GameSystem);

		/**
		 * \brief Game systems (child classes of the GameSystem class) need to
		 * implement this function.
		 */
		virtual void UpdateImpl() = 0;

		/**
		 * \brief Executes this system's logic while measuring the time taken for
		 * profiling purposes.
		 */
		void Update()
		{
			const Clock clock;
			UpdateImpl();

			// Keep track of time elapsed.
			m_timeBuffer[m_runningAvgIdx] = clock.GetElapsedMilliseconds();
			m_runningAvgIdx++;
			if (m_runningAvgIdx >= TIME_BUFFER_SIZE)
				m_runningAvgIdx = 0;
		}

		/**
		 * \return Returns the system name.
		 */
		[[nodiscard]] const std::string& GetName() const
		{
			return m_systemName;
		}

		[[nodiscard]] float ComputeTimeAverage()
		{
			float avg = 0.0f;
			for (const float time : m_timeBuffer)
			{
				avg += time;
			}
			return avg * INV_BUFFER_SIZE;
		}

	protected:
		/**
		 * \brief Registry the system acts on.
		 */
		entt::registry& m_registry;
		/**
		 * \brief Reference to the scene the system acts on. It is the
		 * scene owning the registry of the previous member, which is
		 * only here to simplify calls.
		 */
		Scene& m_scene;

	private:
		std::string m_systemName;

		/**
		 * \brief Number of frames to account for when calculating smoothed averages.
		 */
		constexpr static int TIME_BUFFER_SIZE = 120;
		constexpr static float INV_BUFFER_SIZE = 1.0f / static_cast<float>(TIME_BUFFER_SIZE);
		/**
		 * \brief Running average per frame over the last 120 frames.
		 */
		float m_timeBuffer[TIME_BUFFER_SIZE];
		int m_runningAvgIdx;
	};


	// For later if we have time to work on multithreading.
#if 0
	class ParallelExecutionUnit
	{
	public:
		ParallelExecutionUnit(const std::initializer_list<GameSystem*> systems) :
			m_systems{ systems }
		{
			bool foundNullSys = false;
			for (const GameSystem* system : m_systems)
			{
				if (system == nullptr)
					foundNullSys = true;
			}

			if(foundNullSys)
			{
				for (const GameSystem* system : m_systems)
				{
					delete system;
				}
				m_systems.clear();
				LOG_AND_THROW("Null systems not allowed in a ParallelExecutionUnit.");
			}
		}

		void Execute()
		{
			
		}

	private:
		std::vector<GameSystem*> m_systems;
		std::vector<std::thread> m_threads;
	};



	class SystemsGraph
	{
	public:
		SystemsGraph();

	private:

	};

#endif
}
