#pragma once

#include "BasicComponents.h"
#include "GameSystem.h"

class PlayerControlsSystem final : public Core::GameSystem
{
public:
	PlayerControlsSystem(Scene& scene, entt::registry& registry);
	~PlayerControlsSystem() override;
	DISABLE_COPY(PlayerControlsSystem);
	DISABLE_MOVE(PlayerControlsSystem);

	void UpdateImpl() override;


private:
	void HandleRotation(CTransform& transform, CPlayer& playerComponent) const;
	void HandleTranslation(CTransform& transform, const CPlayer& playerComponent) const;
	void UpdateCameraVectors(CPlayer& playerComponent) const;

	entt::entity m_playerEntity;
	
};
