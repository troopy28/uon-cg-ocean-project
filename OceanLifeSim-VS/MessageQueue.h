#pragma once

#include "Common.h"

#include <queue>
#include <mutex>

/**
 * \brief Minimal thread-safe wrapper around a std::queue. This is used for message-passing
 * between the different threads in the game (an example usage can be found in the LOD
 * system).
 * This class is non-copyable, but default-movable.
 *
 * \tparam MessageT Type of message to pass.
 */
template<typename MessageT>
class MessageQueue
{
public:
	MessageQueue() = default;
	~MessageQueue() = default;
	DISABLE_COPY(MessageQueue);
	DEFAULT_MOVE_CTOR_OP(MessageQueue);

	/**
	 * \brief Adds the given message at the beginning of the queue.
	 * \param message Message to add.
	 * \note This uses a lock under the hood, beware for performances.
	 */
	void Push(MessageT&& message)
	{
		std::lock_guard<std::mutex> guard(m_mutex);
		m_messages.push(message);
	}

	/**
	 * \return Element at the end of the queue if there is any, or
	 * an empty optional if there is not.
	 * \note This uses a lock under the hood, beware for performances.
	 */
	[[nodiscard]] std::optional<MessageT> Pop()
	{
		std::lock_guard<std::mutex> guard(m_mutex);
		if (m_messages.empty())
			return std::nullopt;
		MessageT temp = std::move(m_messages.front());
		m_messages.pop();
		return std::make_optional<MessageT>(std::move(temp));
	}


private:
	/**
	 * \brief Underlying "actual" queue object storing the messages.
	 */
	std::queue<MessageT> m_messages;
	/**
	 * \brief Used for synchronization purposes.
	 */
	std::mutex m_mutex;
};