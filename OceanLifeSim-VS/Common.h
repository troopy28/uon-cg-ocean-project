#pragma once

// Standard includes.
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <functional>
#include <string>
#include <type_traits>

// Entity-component system.
#include <entt/entt.hpp>

// GLFW.
#include <GL/glew.h>  
#include <GLFW/glfw3.h>

// GLM.
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>

// Compression library.

#include <lz4/lz4.h>

// Audio.
#include <fmod_studio.hpp>
#include <fmod_errors.h>

// Logging.
#include "Logging.h"

// Utilities.
#include "Clock.h"
#define MAGIC_ENUM_RANGE_MIN (-128)
#define MAGIC_ENUM_RANGE_MAX 512
#include <magic_enum.hpp>

#define FMOD_ERR_CHECK(x) do{ if((x) != FMOD_OK) LOG_AND_THROW("Audio error occurred: " + std::string(FMOD_ErrorString(x)));} while(false)

#define DISABLE_COPY(ClassName) 	ClassName(const ClassName& other) = delete; \
									ClassName& operator=(const ClassName& other) = delete
#define DISABLE_MOVE(ClassName) 	ClassName(ClassName&& other) = delete; \
									ClassName& operator=(ClassName&& other) = delete
#define DEFAULT_COPY_CTOR_OP(ClassName) 	ClassName(const ClassName& other) = default; \
									ClassName& operator=(const ClassName& other) = default
#define DEFAULT_MOVE_CTOR_OP(ClassName) 	ClassName(ClassName&& other) = default; \
									ClassName& operator=(ClassName&& other) = default

namespace Time
{
	extern volatile float deltaTime;
	extern volatile float time;
	extern volatile unsigned long frame;
}

constexpr glm::vec4 LOCAL_FORWARD{ 0.0f, 0.0f, -1.0f, 0.0f }; // Front vector is the negative Z axis.
constexpr glm::vec4 LOCAL_UP{ 0.0f, 1.0f, 0.0f, 0.0f }; // Up vector is the positive Y axis.
constexpr glm::vec4 LOCAL_RIGHT{ 1.0f, 0.0f, 0.0f, 0.0f }; // Right vector is the positive X axis.

constexpr glm::vec3 WORLD_UP{ 0.0f, 1.0f, 0.0f }; // Up vector is the positive Y axis.
constexpr glm::vec3 ZERO{ 0.0f, 0.0f, 0.0f }; // Up vector is the positive Y axis.

