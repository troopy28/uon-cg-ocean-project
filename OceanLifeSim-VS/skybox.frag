#version 330 core
out vec4 FragColor;

in vec3 TexCoords;

uniform samplerCube skybox;
uniform vec3 viewPos;

void main()
{    
    vec3 background_color = vec3(0.29, 0.38, 0.62) * 0.8;
    float distance = distance(TexCoords, viewPos);

    vec3 result = mix(texture(skybox, TexCoords).rgb, background_color, 0.8);

    FragColor = vec4(result, 1.0);
}