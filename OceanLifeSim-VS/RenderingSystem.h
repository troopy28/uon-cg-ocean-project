#pragma once

#include "BasicComponents.h"
#include "GameSystem.h"
#include "Shader.h"

namespace Core
{
	class GamePlatform;
}

namespace Rendering
{
	class RenderingSystem final : public Core::GameSystem
	{
	public:
		RenderingSystem(Scene& scene, entt::registry& registry, const Core::GamePlatform& gamePlatform);
		~RenderingSystem() override;
		DISABLE_COPY(RenderingSystem);
		DISABLE_MOVE(RenderingSystem);

		void UpdateImpl() override;

		static void SetWireframe(bool useWireframe);

	private:
		void RenderShadowMap(const glm::mat4& lightSpaceMatrix);
		void RenderSimpleMaterial(const glm::mat4& projection, const glm::mat4& view, 
			const glm::vec3& lightPos, const glm::vec3& lightColor, const glm::mat4& lightSpaceMatrix) const;
		void RenderOcean(const glm::mat4& projection, const glm::mat4& view, const glm::vec3& lightPos, const glm::vec3& lightColor) const;
		void RenderSkybox(const glm::mat4& projection, const glm::mat4& view) const;

		void EnsureMeshesInitialized() const;

		Shader m_defaultShader;
		Shader m_oceanShader;
		std::unordered_map<std::string, Shader> shaderMap;
		const Core::GamePlatform& m_gamePlatform;
		Shader testShader;
		Shader skyboxShader;
		Shader m_shadowMapShader;
		GLuint m_shadowMapHandle{};
	};
}
