#pragma once
#include <chrono>

// NOTE (by Maxime Casas): this file is directly copied from a personal project. It was not
// written in the scope of this project, but has its use so I just took it.

namespace Core
{
	class Clock
	{
	public:
		Clock();

		[[nodiscard]] float GetElapsedSeconds() const;
		[[nodiscard]] float GetElapsedMilliseconds() const;
		[[nodiscard]] float GetElapsedMicroseconds() const;

		/**
		 * \brief Resets the clock, so that measurements are now performed compared to the time where this function is called.
		 * \return Elapsed time before the reset in milliseconds.
		 */
		float Reset();

	private:
		std::chrono::steady_clock::time_point m_start;
	};


}