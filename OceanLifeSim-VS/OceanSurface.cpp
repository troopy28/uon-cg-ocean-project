#include "OceanSurface.h"
#include "Scene.h"
#include <fstream>

namespace
{
	constexpr float CHUNK_WORLD_SIZE = 16.0f;
	constexpr float CHUNK_WORLD_SIZE_INV = 1.0f / CHUNK_WORLD_SIZE;
	constexpr int CHUNK_VIEWING_RADIUS = 2;

	constexpr float OCEAN_LEVEL = 30.0f;
}

OceanSurfaceSystem::OceanSurfaceSystem(Scene& scene, entt::registry& registry) :
	GameSystem("Ocean Surface System", scene, registry),
	m_animationFrames(),
	m_currentFrame(0),
	m_tileMeshHandle(AssetLoader::FindOrLoadMeshFromFile(ASSET_MESH_OCEAN_REST_FRAME_FILE_PATH)),
	m_oceanChunkRes(0)
{
	m_oceanChunkRes = LoadOceanAnimation();
}

OceanSurfaceSystem::~OceanSurfaceSystem() = default;

inline int SwapIntEndianess(const int data)
{
	return ((data >> 24) & 0xff) | // move byte 3 to byte 0
		((data << 8) & 0xff0000) | // move byte 1 to byte 2
		((data >> 8) & 0xff00) | // move byte 2 to byte 1
		((data << 24) & static_cast<int>(0xff000000)); // byte 0 to byte 3
}

inline float SwapFloatEndianess(const float inFloat)
{
	float retVal = 0.0f;
	const char* floatToConvert = reinterpret_cast<const char*>(&inFloat);
	char* returnFloat = reinterpret_cast<char*>(&retVal);

	// swap the bytes into a temporary buffer
	returnFloat[0] = floatToConvert[3];
	returnFloat[1] = floatToConvert[2];
	returnFloat[2] = floatToConvert[1];
	returnFloat[3] = floatToConvert[0];

	return retVal;
}

inline int32_t ReadBigEndianInteger(std::ifstream& file)
{
	int32_t value;
	file.read(reinterpret_cast<char*>(&value), sizeof(int32_t));
	return SwapIntEndianess(value);
}

inline float ReadBigEndianFloat(std::ifstream& file)
{
	float value;
	file.read(reinterpret_cast<char*>(&value), sizeof(float));
	return SwapFloatEndianess(value);
}

int OceanSurfaceSystem::LoadOceanAnimation()
{
	// Read the MDD point cache containing the position of the individual vertices at every frame.
	// Note that this format is in big endian... yay... it means we have to swap the bytes of every value we read.
	std::ifstream file(ASSET_MESH_OCEAN_ANIMATION_FILE_PATH, std::ios::binary | std::ios::in);
	if (!file.is_open())
		LOG_AND_THROW(std::string("Cannot open the file ") + ASSET_MESH_OCEAN_ANIMATION_FILE_PATH + " to load the ocean animation.");

	// First the number of frames in the animation as well as the number of vertices is written.
	const int32_t frameCount = ReadBigEndianInteger(file);
	const int32_t vertexCount = ReadBigEndianInteger(file);
	
	// Then the time of each frame. We won't use it, so we just skip this data. The time is encoded using single precision floats.
	file.ignore(static_cast<std::streamsize>(frameCount * sizeof(float)));
	// for (int32_t frameIdx = 0; frameIdx < frameCount; frameIdx++)
	// ReadBigEndianFloat(file); // No use for this value.
	

	// Now the important part: the animation frames. A frame is simply an array of 3D vectors holding the
	// position of the individual points. The size of each frame is therefore vertexCount * 3 * sizeof(float).
	m_animationFrames.resize(frameCount);
	for(int32_t frameIdx = 0; frameIdx < frameCount; frameIdx++)
	{
		std::vector<glm::vec3>& positions = m_animationFrames[frameIdx];
		positions.resize(vertexCount);
		for (int32_t vertexIdx = 0; vertexIdx < vertexCount; vertexIdx++)
		{
			positions[vertexIdx].z = -ReadBigEndianFloat(file);
			positions[vertexIdx].x = ReadBigEndianFloat(file);
			positions[vertexIdx].y = ReadBigEndianFloat(file);
		}
	}

	return static_cast<int>(std::sqrtf(static_cast<float>(vertexCount)));
}

void OceanSurfaceSystem::UpdateImpl()
{
	UpdateOceanSurface();
	ManageTiles();
}


void OceanSurfaceSystem::UpdateOceanSurface()
{
	std::vector<Vertex>& meshVertices = m_tileMeshHandle->vertices;
	const size_t vertexCount = meshVertices.size();
	const std::vector<glm::vec3>& currentFramePositions = m_animationFrames[m_currentFrame];
	for(size_t i = 0; i < vertexCount; i++)
	{
		meshVertices[i].pos = currentFramePositions[i];
		meshVertices[i].normal = { 0.0f, 1.0f, 0.0f }; // In world space.
	}
	m_tileMeshHandle->openglDataInitialized = false;

	m_currentFrame++;
	if (m_currentFrame >= m_animationFrames.size())
		m_currentFrame = 0;
	// LOG_DEBUG("Ocean surface updated. Current animation frame: " + std::to_string(m_currentFrame));
}

void OceanSurfaceSystem::ManageTiles()
{
	// 1. Get the position of the player on the chunk grid.
	const glm::vec3 playerPos = m_scene.GetPlayerPosition();
	const int playerChunkX = static_cast<int>(playerPos.x * CHUNK_WORLD_SIZE_INV);
	const int playerChunkZ = static_cast<int>(playerPos.z * CHUNK_WORLD_SIZE_INV);

	// 2. Setup the list of the chunks that should be loaded.
	std::set<std::pair<int, int>> requiredChunks;
	for (int i = -CHUNK_VIEWING_RADIUS - 1; i <= CHUNK_VIEWING_RADIUS; i++) // Add a chunk in the negative coordinates to make the view more symmetrical.
	{
		for (int j = -CHUNK_VIEWING_RADIUS - 1; j <= CHUNK_VIEWING_RADIUS; j++) // Same here.
		{
			requiredChunks.insert({ i + playerChunkX, j + playerChunkZ });
		}
	}

	// 3. Get the currently loaded chunks (all entities with a COceanSurfaceTile component).
	const auto oceanTileEntities = m_registry.view<const CTransform, COceanSurfaceTile, CMesh>();

	// 4. Remove the chunks already loaded from the required list, and add the ones that require unloading in a list.
	std::vector<entt::entity> chunksToUnload;
	oceanTileEntities.each([&requiredChunks, &chunksToUnload](const entt::entity entity, const CTransform&, COceanSurfaceTile& tile, CMesh&)
		{
			const std::pair<int, int> pos{ tile.chunkPosX, tile.chunkPosZ };
			const auto& it = requiredChunks.find(pos);
			if (it != requiredChunks.end())
			{
				requiredChunks.erase(pos);
			}
			else
			{
				chunksToUnload.push_back(entity);
			}
		});

	// At this point, we know which chunks to unload, and which chunks to generate.

	// 5. Unload the chunks we do not want. This amounts to destroying the corresponding entities.
	for (const entt::entity entity : chunksToUnload)
	{
		const COceanSurfaceTile& ctt = m_registry.get<COceanSurfaceTile>(entity);
		const float chunkPosWorldX = static_cast<float>(ctt.chunkPosX) * CHUNK_WORLD_SIZE;
		const float chunkPosWorldZ = static_cast<float>(ctt.chunkPosZ) * CHUNK_WORLD_SIZE;
		LOG_DEBUG("Unloading ocean tile at " + std::to_string(chunkPosWorldX) + ", " + std::to_string(chunkPosWorldZ));
		m_registry.destroy(entity);
	}

	// 6. Load the chunks we want and that are not here yet.
	for (const auto& pos : requiredChunks)
	{
		const float chunkPosWorldX = static_cast<float>(pos.first) * CHUNK_WORLD_SIZE;
		const float chunkPosWorldZ = static_cast<float>(pos.second) * CHUNK_WORLD_SIZE;
		const entt::entity chunkEntity = m_registry.create();
		m_registry.emplace<CTransform>(chunkEntity, glm::vec3(chunkPosWorldX, OCEAN_LEVEL, chunkPosWorldZ));
		m_registry.emplace<COceanSurfaceTile>(chunkEntity, pos.first, pos.second);

		// Make new tile and give it the same geo handle as the others.
		m_registry.emplace<CMesh>(chunkEntity, m_tileMeshHandle);
		LOG_DEBUG("Loaded ocean tile at " + std::to_string(chunkPosWorldX) + ", " + std::to_string(chunkPosWorldZ));
	}
}
