#pragma once

#include "GameSystem.h"
#include "ShaderProgram.h"
#include "BasicComponents.h"

class RenderSystem final : public Core::GameSystem
{
public:
	RenderSystem(Scene& scene, entt::registry& registry, Shader& defaultShader);
	~RenderSystem() override;
	DISABLE_COPY(RenderSystem);
	DISABLE_MOVE(RenderSystem);

	void UpdateImpl() override;
private: 
	
};