#include "AssetLoader.h"

#include <fstream>
#include <filesystem>
#include <mutex>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

namespace Core
{
	template<typename T>
	char* CompressVector(std::vector<T> vec, int& compressedDataSize)
	{
		const int sourceDataSize = static_cast<int>(vec.size() * sizeof(T));
		const int maxDestinationSize = LZ4_compressBound(sourceDataSize);
		char* compressedData = static_cast<char*>(malloc(static_cast<size_t>(maxDestinationSize)));
		if (compressedData == nullptr)
			LOG_AND_THROW("Malloc failed for compressed array.");

		const char* sourceData = reinterpret_cast<const char*>(vec.data());
		compressedDataSize = LZ4_compress_default(sourceData, compressedData, sourceDataSize, maxDestinationSize);
		if (compressedDataSize <= 0)
			LOG_AND_THROW("Compression failed.");

		// ratio = compressed_data_size/src_size
		// maybe: compressed_data = (char *)realloc(compressed_data, (size_t)compressed_data_size); : REALLOC TO SAVE SPACE. USEFUL?

		return compressedData;
	}

	/**
	 * \brief Decompresses the given data into a vector of the given element type, of the given size.
	 * \tparam T Type of the data stored in the vector.
	 * \param compressedData Pointer to the compressed data to build the vector from.
	 * \param compressedDataSize Size of the compressed array.
	 * \param targetVectorSize Number of elements in the vector. This information cannot be retrieved from the data and needs to be provided.
	 * \return Vector from the compressed data.
	 */
	template<typename T>
	std::vector<T> DecompressAsVector(char* compressedData, const int compressedDataSize, const size_t targetVectorSize)
	{
		std::vector<T> vec(targetVectorSize);
		char* uncompressedDataTarget = reinterpret_cast<char*>(vec.data());
		const int vectorByteSize = static_cast<int>(targetVectorSize * sizeof(T));
		LZ4_decompress_safe(compressedData, uncompressedDataTarget, compressedDataSize, vectorByteSize);
		return vec; // note: copy elision
	}

	MeshData::MeshData() :
		vertices(),
		indices(),
		meshName("Empty mesh - DEFAULT CONSTRUCTED"),
		vao(0),
		vbo(0),
		ebo(0)
	{
	}

	MeshData::MeshData(std::vector<Vertex> _vertices, std::vector<uint32_t> _indices) :
		vertices(std::move(_vertices)),
		indices(std::move(_indices))
	{
	}

	MeshData::MeshData(MeshData&& other) noexcept :
		vertices(std::move(other.vertices)),
		indices(std::move(other.indices)),
		meshName(std::move(other.meshName)),
		vao(other.vao),
		vbo(other.vbo),
		ebo(other.ebo)
	{
		other.vao = 0;
		other.vbo = 0;
		other.ebo = 0;
	}

	MeshData& MeshData::operator=(MeshData&& other) noexcept
	{
		vertices = std::move(other.vertices);
		indices = std::move(other.indices);
		meshName = std::move(other.meshName);
		vao = other.vao;
		vbo = other.vbo;
		ebo = other.ebo;
		other.vao = 0;
		other.vbo = 0;
		other.ebo = 0;
		return *this;
	}

	MeshData::~MeshData()
	{
		glDeleteVertexArrays(1, &vao);
		glDeleteBuffers(1, &vbo);
		glDeleteBuffers(1, &ebo);
	}

	void MeshData::SetupOpenGlData()
	{
		// FIRST THINGS FIRST: if there is already some active opengl handles, free them.
		if (vao != 0)
			glDeleteVertexArrays(1, &vao);
		if (vbo != 0)
			glDeleteBuffers(1, &vbo);
		if (ebo != 0)
			glDeleteBuffers(1, &ebo);

		// now that we have all the required data, set the vertex buffers and its attribute pointers.
		// create buffers/arrays
		glGenVertexArrays(1, &vao);
		glGenBuffers(1, &vbo);
		glGenBuffers(1, &ebo);

		glBindVertexArray(vao);
		// load data into vertex buffers
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		// A great thing about structs is that their memory layout is sequential for all its items.
		// The effect is that we can simply pass a pointer to the struct and it translates perfectly to a glm::vec3/2 array which
		// again translates to 3/2 floats which translates to a byte array.
		glBufferData(GL_ARRAY_BUFFER, static_cast<GLsizeiptr>(vertices.size() * sizeof(Vertex)), vertices.data(), GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, static_cast<GLsizeiptr>(indices.size() * sizeof(uint32_t)), indices.data(), GL_STATIC_DRAW);

		// set the vertex attribute pointers
		// vertex Positions
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), nullptr);
		// vertex normals
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<const void*>(offsetof(Vertex, normal)));  // NOLINT(performance-no-int-to-ptr) -- we HAVE to cast or it doesn't compile... clang is dumb.
		// vertex tangents
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<const void*>(offsetof(Vertex, tangent)));  // NOLINT(performance-no-int-to-ptr) -- we HAVE to cast or it doesn't compile... clang is dumb.
		// vertex bitangents
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<const void*>(offsetof(Vertex, bitangent)));  // NOLINT(performance-no-int-to-ptr) -- we HAVE to cast or it doesn't compile... clang is dumb.
		// vertex texture coords
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<const void*>(offsetof(Vertex, uv)));  // NOLINT(performance-no-int-to-ptr) -- same.

		glBindVertexArray(0);
		openglDataInitialized = true;
	}


}



const std::string& AssetLoader::GetTextFile(const std::string& filePath)
{
	static std::unordered_map<std::string, std::string> cachedFiles{};

	// If the file has already been loaded, just retrieve it from the cache.
	if (const auto cachedResult = cachedFiles.find(filePath); cachedResult != cachedFiles.end())
		return cachedResult->second;

	// If not, load it, store it in the cache, and return the cached value.
	std::ifstream in(filePath);
	const std::string contents((std::istreambuf_iterator<char>(in)),
		std::istreambuf_iterator<char>());
	cachedFiles.emplace(filePath, contents);
	return cachedFiles[filePath];
}

const Texture& AssetLoader::GetTexture(const std::string& filePath)
{
	using MapT = std::unordered_map<std::string, Texture>;
	static MapT cachedTextures{};

	// If the file has already been loaded, just retrieve it from the cache.
	if (const auto cachedResult = cachedTextures.find(filePath); cachedResult != cachedTextures.end())
		return cachedResult->second;

	int width, height, nbChannels;
	const unsigned char* data = stbi_load(filePath.c_str(), &width, &height, &nbChannels, 0);
	const int glChannel = nbChannels == 3 ? GL_RGB : GL_RGBA;
	cachedTextures.insert(
		// Gotta help the compiler a bit here by specifying the MapT::value_type type.
		// (Initializer-list's type resolution is confused otherwise).
		MapT::value_type(filePath, Texture{ width, height, glChannel, data })
	);

	return cachedTextures[filePath];
}

static std::unordered_map<std::string, Core::MeshData> loadedMeshes{};
static std::unordered_map<const Core::MeshData*, int> meshUserCount{};
namespace
{
	std::recursive_mutex  meshDataMutex{};
}

void NotifyMeshInUse(const Core::MeshData* meshData, const std::string& reason)
{
	if (meshData == nullptr)
		return;

	std::lock_guard<std::recursive_mutex> lockGuard(meshDataMutex);
	++meshUserCount[meshData];
	// LOG_DEBUG("Mesh " + meshData->meshName + " has a new user. User count: " + std::to_string(meshUserCount[meshData]) + " Reason: " + reason);
}

void NotifyMeshUnused(const Core::MeshData* meshData, const std::string& reason)
{
	if (meshData == nullptr)
		return;

	std::lock_guard<std::recursive_mutex> lockGuard(meshDataMutex);
	const int newCount = --meshUserCount[meshData];
	// LOG_DEBUG("Mesh " + meshData->meshName + " lost a user. User count: " + std::to_string(newCount) + " Reason: " + reason);

	if (newCount == 0)
	{
		// LOG_DEBUG("Mesh " + meshData->meshName + " is unused. Removing it from the mesh cache.");
		loadedMeshes.erase(meshData->meshName); // Frees the memory.
		meshUserCount.erase(meshData); // Remove it from the secondary hashmap too.
	}
}

MeshDataHandle AssetLoader::FindOrLoadMeshFromFile(const std::string& filePath)
{
	std::lock_guard<std::recursive_mutex> lockGuard(meshDataMutex);
	// LOG_DEBUG("Getting mesh " + filePath);

	const auto& findRes = loadedMeshes.find(filePath);
	if (findRes != loadedMeshes.end())
	{
		// LOG_DEBUG("Mesh " + filePath + " was found in the cache. No file IO performed, returning pointer to the cache entry.");
		return MeshDataHandle{ &findRes->second, NotifyMeshInUse, NotifyMeshUnused };
	}

	LOG_DEBUG("Mesh " + filePath + " not in the cache. IO will be performed.");

	std::ifstream file(filePath, std::ios::in | std::ios::binary);
	if (!file.is_open())
		LOG_AND_THROW("Cannot open the mesh file at " + filePath);

	// Vertices.
	std::vector<Vertex> vertices{};
	size_t vertexCount = 0;
	file.read(reinterpret_cast<char*>(&vertexCount), sizeof(size_t));
	vertices.resize(vertexCount);
	file.read(reinterpret_cast<char*>(vertices.data()), static_cast<std::streamsize>(vertexCount * sizeof(Vertex)));

	// Indices.
	std::vector<uint32_t> indices{};
	size_t indexCount = 0;
	file.read(reinterpret_cast<char*>(&indexCount), sizeof(size_t));
	indices.resize(indexCount);
	file.read(reinterpret_cast<char*>(indices.data()), static_cast<std::streamsize>(indexCount * sizeof(uint32_t)));

	// Insert the mesh in the cache structures with a count of 0. The handle's constructor
	// will increase that count to 1.
	Core::MeshData mesh{ vertices, indices };
	loadedMeshes.insert({ filePath, std::move(mesh) }); // note: it is required to specify the pair type, or it doesn't compile.
	Core::MeshData* meshDataPtr = &loadedMeshes.at(filePath);
	meshUserCount[meshDataPtr] = 0;
	meshDataPtr->meshName = std::filesystem::path(filePath).generic_string();
	return MeshDataHandle{ meshDataPtr, NotifyMeshInUse, NotifyMeshUnused }; // Copy elision.
}

MeshDataHandle AssetLoader::FindOrAllocateMesh(const std::string& name)
{
	std::lock_guard<std::recursive_mutex> lockGuard(meshDataMutex);


	const auto& findRes = loadedMeshes.find(name);
	if (findRes != loadedMeshes.end())
	{
		// LOG_DEBUG("Mesh " + name + " was found in the cache. No allocation performed, returning pointer to the cache entry.");
		return MeshDataHandle{ &findRes->second, NotifyMeshInUse, NotifyMeshUnused };
	}

	Core::MeshData mesh{};
	mesh.meshName = name;
	loadedMeshes.insert({ name, std::move(mesh) }); // note: it is required to specify the pair type, or it doesn't compile.
	Core::MeshData* meshDataPtr = &loadedMeshes.at(name);
	meshUserCount[meshDataPtr] = 0;
	return MeshDataHandle{ meshDataPtr, NotifyMeshInUse, NotifyMeshUnused }; // Copy elision.
}
