#include "RenderingSystem.h"
#include "Scene.h"
#include "GamePlatform.h"
#include "stb_image.h"

namespace Rendering
{
	constexpr GLsizei SHW_SHADOW_MAP_WIDTH = 1024, SHADOW_MAP_HEIGHT = 1024;

	float vertices[] = {
		-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
		 0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
		-0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,

		-0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
		 0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,

		-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
		-0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
		-0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
		-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,

		 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
		 0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
		 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
		 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
		 0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
		 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

		-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
		 0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
		 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
		 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,

		-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
		-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f
	};

	unsigned int VBO, cubeVAO;

	float skyboxVertices[] =
	{
		//   Coordinates
		-1.0f, -1.0f,  1.0f,//        7--------6
		 1.0f, -1.0f,  1.0f,//       /|       /|
		 1.0f, -1.0f, -1.0f,//      4--------5 |
		-1.0f, -1.0f, -1.0f,//      | |      | |
		-1.0f,  1.0f,  1.0f,//      | 3------|-2
		 1.0f,  1.0f,  1.0f,//      |/       |/
		 1.0f,  1.0f, -1.0f,//      0--------1
		-1.0f,  1.0f, -1.0f
	};

	unsigned int skyboxIndices[] =
	{
		// Right
		1, 2, 6,
		6, 5, 1,
		// Left
		0, 4, 7,
		7, 3, 0,
		// Top
		4, 5, 6,
		6, 7, 4,
		// Bottom
		0, 3, 2,
		2, 1, 0,
		// Back
		0, 1, 5,
		5, 4, 0,
		// Front
		3, 7, 6,
		6, 2, 3
	};

	unsigned int skyboxVAO, skyboxVBO, skyboxEBO, cubemapTexture;

	unsigned int shadowMapFBO;



	RenderingSystem::RenderingSystem(Scene& scene, entt::registry& registry, const Core::GamePlatform& gamePlatform) :
		GameSystem("Rendering System", scene, registry),
		m_defaultShader(),
		m_oceanShader(AssetLoader::GetTextFile(ASSET_SHADER_OCEAN_VERTEX),
		              AssetLoader::GetTextFile(ASSET_SHADER_OCEAN_FRAG)),
		m_gamePlatform(gamePlatform),
		//Shadow Map Shader
		testShader(
			AssetLoader::GetTextFile(ASSET_SHADER_SINGLE_COLOR_VERTEX),
			AssetLoader::GetTextFile(ASSET_SHADER_SINGLE_COLOR_FRAG)),

		skyboxShader(AssetLoader::GetTextFile(ASSET_SHADER_SKYBOX_VERTEX),
		             AssetLoader::GetTextFile(ASSET_SHADER_SKYBOX_FRAG)),
		m_shadowMapShader(AssetLoader::GetTextFile(ASSET_SHADER_SHADOW_MAP_VERTEX),
		                  AssetLoader::GetTextFile(ASSET_SHADER_SHADOW_MAP_FRAG))
	{
		// Setup OpenGL.

		//glClearColor(0.5f, 0.7f, 1.0f, 1.0f);
		// configure global opengl state
		// -----------------------------
		glEnable(GL_DEPTH_TEST);
		SetWireframe(false); // testShader

		//Shadow Map Framebuffer
		{
			glGenFramebuffers(1, &shadowMapFBO);

			glGenTextures(1, &m_shadowMapHandle);
			glBindTexture(GL_TEXTURE_2D, m_shadowMapHandle);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHW_SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			
			glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_shadowMapHandle, 0);
			glDrawBuffer(GL_NONE);
			glReadBuffer(GL_NONE);
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}

		// Skybox
		{
			glGenVertexArrays(1, &skyboxVAO);
			glGenBuffers(1, &skyboxVBO);
			glGenBuffers(1, &skyboxEBO);
			glBindVertexArray(skyboxVAO);
			glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skyboxEBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(skyboxIndices), &skyboxIndices, GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
			glEnableVertexAttribArray(0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

			std::string facesCubemap[6] =
			{
				"assets/textures/skybox/right.jpg",
				"assets/textures/skybox/left.jpg",
				"assets/textures/skybox/top.jpg",
				"assets/textures/skybox/bottom.jpg",
				"assets/textures/skybox/front.jpg",
				"assets/textures/skybox/back.jpg"
			};

		
			glGenTextures(1, &cubemapTexture);
			glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			// These are very important to prevent seams
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
			// This might help with seams on some systems
			glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

			// Cycles through all the textures and attaches them to the cubemap object
			for (unsigned int i = 0; i < 6; i++)
			{
				int width, height, nrChannels;
				unsigned char* data = stbi_load(facesCubemap[i].c_str(), &width, &height, &nrChannels, 0);
				if (data)
				{
					stbi_set_flip_vertically_on_load(false);
					glTexImage2D
					(
						GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
						0,
						GL_RGB,
						width,
						height,
						0,
						GL_RGB,
						GL_UNSIGNED_BYTE,
						data
					);
					stbi_image_free(data);
				}
				else
				{
					std::cout << "Failed to load texture: " << facesCubemap[i] << std::endl;
					stbi_image_free(data);
				}
			}
		}

		{
			glGenVertexArrays(1, &cubeVAO);
			glGenBuffers(1, &VBO);

			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

			glBindVertexArray(cubeVAO);

			// position attribute
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
			glEnableVertexAttribArray(0);
			// normal attribute
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
			glEnableVertexAttribArray(1);


			// second, configure the light's VAO (VBO stays the same; the vertices are the same for the light object which is also a 3D cube)
			unsigned int lightCubeVAO;
			glGenVertexArrays(1, &lightCubeVAO);
			glBindVertexArray(lightCubeVAO);

			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			// note that we update the lamp's position attribute's stride to reflect the updated buffer data
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
			glEnableVertexAttribArray(0);
		}
	}

	RenderingSystem::~RenderingSystem()
	{
	}

	void setupTexture(const Texture* texture, const int textureUnit)
	{
		// todo: check if texture is nullptr. could be interesting to allow that in materials
		// todo: and replace those with a fully white or fully black texture

		glActiveTexture(GL_TEXTURE0 + textureUnit); // active proper texture unit before binding
		glBindTexture(GL_TEXTURE_2D, texture->glId);
	}

	void TestRender(const Shader& testShader,
		const glm::vec3 lightPos,
		const glm::vec3 lightColor,
		const glm::vec3 playerPosition,
		const glm::mat4 projection,
		const glm::mat4 view)
	{
		testShader.use();
		testShader.setVec3("objectColor", 1.0f, 0.0f, 0.0f);
		testShader.setVec3("lightColor", lightColor);
		testShader.setVec3("lightPos", lightPos);
		testShader.setVec3("viewPos", playerPosition);

		// view/projection transformations
		testShader.setMat4("projection", projection);
		testShader.setMat4("view", view);

		glBindVertexArray(cubeVAO);

		for (int i = 0; i < 10; i++)
		{
			for (int j = 0; j < 10; j++)
			{
				// world transformation
				glm::mat4 model = glm::mat4(1.0f);
				model = glm::translate(model, glm::vec3(
					static_cast<float>(2 * i),
					std::sinf(Time::time * 0.3f + static_cast<float>(i)) + std::cosf(Time::time * 0.3f + static_cast<float>(j)),
					static_cast<float>(2 * j)));
				model = glm::rotate(model, static_cast<float>(i + j) * Time::time * 0.05f, WORLD_UP);
				testShader.setMat4("model", model);
				// render the cube
				glDrawArrays(GL_TRIANGLES, 0, 36);
			}
		}
	}

	void RenderingSystem::UpdateImpl()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		if (Input::IsKeyDown(Core::KeyCode::O))
			SetWireframe(true);
		if (Input::IsKeyDown(Core::KeyCode::P))
			SetWireframe(false);

		// 1. Ensure all the meshes in the scene are properly initialized.
		EnsureMeshesInitialized();

		// 2. Get the data to inject in shaders.
		const CCamera cameraComponent = m_scene.GetCamera();
		const float aspectRatio = m_gamePlatform.GetAspectRatio();
		const glm::mat4 cameraProjection = glm::perspective(cameraComponent.fov, aspectRatio, 0.1f, 100.0f);
		const glm::mat4 cameraView = cameraComponent.viewMatrix;

		
		const auto lights = m_registry.view<const CTransform, const CDirectionalLight>();
		const auto& sunLight = lights.front();
		const glm::vec3 lightPos = {1500.0f, 1000.0f, 5.0f};
		const glm::vec3 lightColor = m_registry.get<CDirectionalLight>(sunLight).color;
		constexpr float lightNearPlane = 0.1f;
		constexpr float lightFarPlane = 1000.0f;
		const glm::mat4 lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, lightNearPlane, lightFarPlane);
		const glm::mat4 lightView = glm::lookAt(lightPos, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
		const glm::mat4 lightSpaceMatrix = lightProjection * lightView;

		//TestRender(testShader, lightDir, lightColor, m_scene.GetPlayerPosition(), projection, view);

		// 3. Render the scene from the perspective of the light for shadow mapping.
		RenderShadowMap(lightSpaceMatrix);

		// 4. One render pass per shader type.
		RenderSimpleMaterial(cameraProjection, cameraView, lightPos, lightColor, lightSpaceMatrix);
		RenderOcean(cameraProjection, cameraView, lightPos, lightColor);
		RenderSkybox(cameraProjection, glm::mat4(glm::mat3(cameraView)));
	}

	void RenderingSystem::RenderSkybox(const glm::mat4& projection, const glm::mat4& view) const
	{
		// Since the cubemap will always have a depth of 1.0, we need that equal sign so it doesn't get discarded
		glDepthFunc(GL_LEQUAL);

		skyboxShader.use();
		skyboxShader.setMat4("projection", projection);
		skyboxShader.setMat4("view", view);
		skyboxShader.setInt("skybox", 0);
		skyboxShader.setVec3("viewPos", m_scene.GetPlayerPosition());

		glBindVertexArray(skyboxVAO);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);
		glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
		glDepthFunc(GL_LESS);
	}

	void RenderingSystem::RenderSimpleMaterial(const glm::mat4& projection, const glm::mat4& view,
		const glm::vec3& lightPos, const glm::vec3& lightColor, const glm::mat4& lightSpaceMatrix) const
	{
		// 2. Set up the shader.
		m_defaultShader.use();
		m_defaultShader.setMat4("projection", projection);
		m_defaultShader.setMat4("view", view);
		m_defaultShader.setVec3("lightColor", lightColor);
		m_defaultShader.setVec3("lightPos", lightPos);
		m_defaultShader.setVec3("viewPos", m_scene.GetPlayerPosition());
		m_defaultShader.setMat4("lightSpaceMatrix", lightSpaceMatrix);

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, m_shadowMapHandle);

		const auto simpleMaterialEntities = m_registry.view<const CTransform, const CSimpleMaterial, CMesh>(entt::exclude<COceanSurfaceTile>);
		simpleMaterialEntities.each([this](const CTransform& transform, const CSimpleMaterial& material, const CMesh& mesh)
			{
				// Set the model matrix.
				glm::mat4 modelMat(1.0f);
				modelMat = glm::translate(modelMat, transform.position);
				modelMat *= glm::eulerAngleXYZ(glm::radians(transform.rotation.x), glm::radians(transform.rotation.y), glm::radians(transform.rotation.z)); // translate it down so it's at the center of the scene
				modelMat = glm::scale(modelMat, transform.scale);
				m_defaultShader.setMat4("model", modelMat);

				// Setup the textures.
				setupTexture(material.diffuseMap, 0);
				setupTexture(material.normalMap, 1);
				setupTexture(material.specularMap, 2);


				// draw mesh
				glBindVertexArray(mesh.meshData->vao);
				glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(mesh.meshData->indices.size()), GL_UNSIGNED_INT, nullptr);
				glBindVertexArray(0);

				// always good practice to set everything back to defaults once configured.
				glActiveTexture(GL_TEXTURE0);

				// LOG_DEBUG(mesh.meshData->meshName + " drawn.");
			});
	}

	void RenderingSystem::RenderOcean(const glm::mat4& projection, const glm::mat4& view, const glm::vec3& lightPos,
		const glm::vec3& lightColor) const
	{	
		glEnable(GL_BLEND);
		m_oceanShader.use();
		m_oceanShader.setMat4("projection", projection);
		m_oceanShader.setMat4("view", view);
		m_oceanShader.setVec3("lightColor", lightColor);
		m_oceanShader.setVec3("lightPos", lightPos);
		m_oceanShader.setVec3("viewPos", m_scene.GetPlayerPosition());

		const auto simpleMaterialEntities = m_registry.view<const CTransform, CMesh, COceanSurfaceTile>();
		
		if(m_registry.valid(simpleMaterialEntities.front()))
		{
			
			simpleMaterialEntities.each([this](const CTransform& transform, CMesh& mesh, COceanSurfaceTile&)
				{
					// Set the model matrix.
					glm::mat4 modelMat(1.0f);
					modelMat = glm::translate(modelMat, transform.position);
					modelMat = glm::scale(modelMat, transform.scale);
					m_oceanShader.setMat4("model", modelMat);

					// draw mesh
					glBindVertexArray(mesh.meshData->vao);
					glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(mesh.meshData->indices.size()), GL_UNSIGNED_INT, nullptr);
					glBindVertexArray(0);
				});
		}
		glDisable(GL_BLEND);
	}


	void RenderingSystem::SetWireframe(const bool useWireframe)
	{
		glPolygonMode(GL_FRONT_AND_BACK, useWireframe ? GL_LINE : GL_FILL);
	}

	void RenderingSystem::RenderShadowMap(const glm::mat4& lightSpaceMatrix)
	{
		m_shadowMapShader.use();
		m_shadowMapShader.setMat4("lightSpaceMatrix", lightSpaceMatrix);

		glViewport(0, 0, SHW_SHADOW_MAP_WIDTH, SHADOW_MAP_HEIGHT);
		glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
		glClear(GL_DEPTH_BUFFER_BIT);
		glActiveTexture(GL_TEXTURE0);
		
		const auto simpleMaterialEntities = m_registry.view<const CTransform, const CSimpleMaterial, CMesh>();
		simpleMaterialEntities.each([this](const CTransform& transform, const CSimpleMaterial&, const CMesh& mesh)
			{
				// Set the model matrix.
				glm::mat4 modelMat(1.0f);
				modelMat = glm::translate(modelMat, transform.position);
				modelMat *= glm::eulerAngleXYZ(glm::radians(transform.rotation.x), glm::radians(transform.rotation.y), glm::radians(transform.rotation.z)); // translate it down so it's at the center of the scene
				modelMat = glm::scale(modelMat, transform.scale);
				m_shadowMapShader.setMat4("model", modelMat);

				// draw mesh
				glBindVertexArray(mesh.meshData->vao);
				glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(mesh.meshData->indices.size()), GL_UNSIGNED_INT, nullptr);
				glBindVertexArray(0);

				// always good practice to set everything back to defaults once configured.
				glActiveTexture(GL_TEXTURE0);

				// LOG_DEBUG(mesh.meshData->meshName + " drawn.");
			});

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		glViewport(0, 0, m_gamePlatform.GetWidth<GLsizei>(), m_gamePlatform.GetHeight<GLsizei>());
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void RenderingSystem::EnsureMeshesInitialized() const
	{
		m_registry
			.view<const CTransform, CMesh>()
			.each([this](const CTransform& transform, CMesh& mesh)
				{
					if (!mesh.meshData->openglDataInitialized)
						mesh.meshData->SetupOpenGlData();
				});
	}
}
