#include "Clock.h"

// NOTE (by Maxime Casas): this file is directly copied from a personal project. It was not
// written in the scope of this project, but has its use so I just took it.

namespace Core
{
	constexpr float INV_MICROSECONDS_IN_ONE_SECOND = 1.0f / 1000000.0f;
	constexpr float INV_MS_IN_ONE_SECOND = 1.0f / 1000.0f;

	Clock::Clock() :
		m_start(std::chrono::steady_clock::now())
	{
	}

	float Clock::GetElapsedSeconds() const
	{
		const auto currentTime = std::chrono::steady_clock::now();
		return static_cast<float>(std::chrono::duration_cast<std::chrono::microseconds>(currentTime - m_start).count()) * INV_MICROSECONDS_IN_ONE_SECOND;
	}

	float Clock::GetElapsedMilliseconds() const
	{
		const auto currentTime = std::chrono::steady_clock::now();
		return static_cast<float>(std::chrono::duration_cast<std::chrono::microseconds>(currentTime - m_start).count()) * INV_MS_IN_ONE_SECOND;
	}

	float Clock::GetElapsedMicroseconds() const
	{
		const auto currentTime = std::chrono::steady_clock::now();
		return static_cast<float>(std::chrono::duration_cast<std::chrono::microseconds>(currentTime - m_start).count());
	}

	float Clock::Reset()
	{
		const float elapsedMs = GetElapsedMilliseconds();
		m_start = std::chrono::steady_clock::now();
		return elapsedMs;
	}

}


