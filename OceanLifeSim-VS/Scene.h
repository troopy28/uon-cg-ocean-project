#pragma once

#include "BasicComponents.h"
#include "Common.h"
#include "GameSystem.h"

class Scene
{
public:
	explicit Scene(std::string sceneName);

	/**
	 * \return Registry of the scene, ie the data structure storing the entities and their components.
	 * Read-only getter.
	 */
	[[nodiscard]] const entt::registry& GetRegistry() const;
	/**
	 * \return Registry of the scene, ie the data structure storing the entities and their components.
	 * Read-write getter.
	 */
	[[nodiscard]] entt::registry& GetRegistry();

	/**
	 * \brief Adds a system of the given type to the scene.
	 * \tparam SystemT Type of the system.
	 * \tparam Args Types of the different arguments the system takes besides the usual scene and registry references.
	 * This template argument is usually automatically resolved.
	 * \param systemArgs Arguments the system takes besides the usual scene and registry references.
	 */
	template<typename SystemT, typename ...Args>
	void RegisterSystem(Args&&... systemArgs)
	{
		try
		{
			m_systems.emplace_back(std::make_unique<SystemT>(
				*this,
				m_registry,
				std::forward<Args>(systemArgs)...)
			);
		}
		catch (std::runtime_error&)
		{
			LOG_ERR(std::string(typeid(SystemT).name()) + " not loaded.");
		}
	}
	/**
	 * \brief Updates all the systems registered in the scene.
	 */
	void Update();

	/**
	 * \return Entity corresponding to the player. It has a camera and a transform.
	 */
	[[nodiscard]] entt::entity GetPlayerEntity() const;
	/**
	 * \brief Shorthand for getting the position of the player entity.
	 * \return Position of the player entity.
	 */
	[[nodiscard]] glm::vec3 GetPlayerPosition() const;

	[[nodiscard]] const CCamera& GetCamera() const;

	template<typename T, typename... Args>
	T& AddComponent(entt::entity entity, Args&&... args)
	{
		return m_registry.emplace<T>(entity, std::forward<Args>(args)...);
	}

private:
	std::string m_sceneName;
	entt::registry m_registry;
	std::vector<std::unique_ptr<Core::GameSystem>> m_systems;
	/**
	 * \brief The player entity has a camera, a player controller, and later
	 * some other stuff too.
	 */
	entt::entity m_playerEntity;
};
