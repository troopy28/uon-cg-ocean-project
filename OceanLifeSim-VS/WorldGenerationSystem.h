#pragma once

#include "BasicComponents.h"
#include "GameSystem.h"
#include "PerlinNoise.h"

class WorldGenerationSystem final : public Core::GameSystem
{
public:
	WorldGenerationSystem(Scene& scene, entt::registry& registry);
	~WorldGenerationSystem() override;
	DISABLE_COPY(WorldGenerationSystem);
	DISABLE_MOVE(WorldGenerationSystem);

	void UpdateImpl() override;

private:
	void GenerateChunk(float chunkPosWorldX, float chunkPosWorldZ, MeshDataHandle& meshDataHandle) const;
	void GenerateScatter(float chunkPosWorldX, float chunkPosWorldZ) const;

	PerlinNoise m_perlinNoise, scatterNoise;
};
