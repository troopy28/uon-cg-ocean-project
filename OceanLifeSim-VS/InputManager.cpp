#include "InputManager.h"

namespace Core
{
	InputManager::InputManager() :
		m_pressedKeys(),
		m_downKeys(),
		m_releasedKeys(),
		m_mouseX(0.0f),
		m_mouseY(0.0f),
		m_mouseDx(0.0f),
		m_mouseDy(0.0f)
	{
		if (s_instance != nullptr)
			LOG_AND_THROW("InputManager cannot be instanced several times.");
		s_instance = this;
		LOG_DEBUG("InputManager initialized. Singleton instance set.");
	}

	void InputManager::Clear()
	{
		m_pressedKeys.clear();
		m_releasedKeys.clear();
		m_mouseDx = 0.0f;
		m_mouseDy = 0.0f;
	}

	void InputManager::OnKeyChanged(const int key, const int scanCode, const int action, const int mods)
	{
		const KeyCode keyCode = static_cast<KeyCode>(key);
		if (action == GLFW_PRESS)
		{
			m_pressedKeys.insert(keyCode);
			m_downKeys.insert(keyCode);
		}
		else if (action == GLFW_RELEASE)
		{
			m_downKeys.erase(keyCode);
			m_releasedKeys.insert(keyCode);
		}
	}

	void InputManager::DebugPrintKeys() const
	{
		if (m_pressedKeys.empty() && m_releasedKeys.empty())
			return;

		std::cout << "InputManager Keys: ";
		std::cout << "Pressed = [ ";
		for(const KeyCode& pressedKey : m_pressedKeys)
		{
			std::cout << magic_enum::enum_name(pressedKey) << " ";
		}
		std::cout << "] Down = [ ";
		for (const KeyCode& downKey : m_downKeys)
		{
			std::cout << magic_enum::enum_name(downKey) << " ";
		}
		std::cout << "] Released = [ ";
		for (const KeyCode& releasedKey : m_releasedKeys)
		{
			std::cout << magic_enum::enum_name(releasedKey) << " ";
		}
		std::cout << "]" << std::endl;
	}

	void InputManager::OnCursorMoved(const double xPos, const double yPos)
	{
		const float x = static_cast<float>(xPos);
		const float y = static_cast<float>(yPos);
		m_mouseDx = x - m_mouseX;
		m_mouseDy = y - m_mouseY;
		m_mouseX = x;
		m_mouseY = y;
	}

	bool InputManager::IsKeyPressed(const KeyCode key)
	{
		return s_instance->m_pressedKeys.find(key) != s_instance->m_pressedKeys.end();
	}

	bool InputManager::IsKeyDown(const KeyCode key)
	{
		return s_instance->m_downKeys.find(key) != s_instance->m_downKeys.end();
	}

	bool InputManager::IsKeyReleased(const KeyCode key)
	{
		return s_instance->m_releasedKeys.find(key) != s_instance->m_releasedKeys.end();
	}

	float InputManager::GetMouseX()
	{
		return s_instance->m_mouseX;
	}

	float InputManager::GetMouseY()
	{
		return s_instance->m_mouseY;
	}

	float InputManager::GetMouseDeltaX()
	{
		return s_instance->m_mouseDx;
	}

	float InputManager::GetMouseDeltaY()
	{
		return -s_instance->m_mouseDy; // Reverse this, because y-coordinates go from bottom to top.
	}
}
