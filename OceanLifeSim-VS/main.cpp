#include "GamePlatform.h"
#include "Scene.h"
#include "OceanSurface.h"
#include "LODSystem.h"
#include "PlayerControlsSystem.h"
#include "AudioSystem.h"
#include "RenderingSystem.h"
#include "WorldGenerationSystem.h"

void SetupTestScene(Scene& scene)
{
	entt::registry& reg = scene.GetRegistry();

	// 1. Create pillar object, using LOD and mesh components.
	{
		const entt::entity ent = reg.create();
		reg.emplace<CTransform>(ent, glm::vec3(-1.0f, 0.0f, -1.0f), ZERO, glm::vec3(0.03f));
		const CLevelsOfDetails& lod = reg.emplace<CLevelsOfDetails>(ent, new LODData{
			{
				LODLevel{ASSET_MESH_PILLAR1_LOD0, 3.0f * 3.0f},
				LODLevel{ASSET_MESH_PILLAR1_LOD1, 4.0f * 4.0f},
				LODLevel{ASSET_MESH_PILLAR1_LOD2, 8.0f * 8.0f},
				LODLevel{ASSET_MESH_PILLAR1_LOD3, 10.0f * 10.0f},
				LODLevel{ASSET_MESH_PILLAR1_LOD4, 12.0f * 12.0f},
				LODLevel{ASSET_MESH_PILLAR1_LOD5, 15.0f * 15.0f},
			},5 });
		reg.emplace<CMesh>(ent, AssetLoader::FindOrLoadMeshFromFile(lod.lodData->levels[lod.lodData->currentLevel].path));
		reg.emplace<CSimpleMaterial>(ent,
			&AssetLoader::GetTexture(ASSET_MESH_PILLAR1_DIFFUSE),
			&AssetLoader::GetTexture(ASSET_MESH_PILLAR1_NORMALS),
			&AssetLoader::GetTexture(ASSET_MESH_PILLAR1_ROUGHNESS));
	}

	// 2. Create mossy rock 1 object, using LOD and mesh components.
	{
		const entt::entity ent = reg.create();
		reg.emplace<CTransform>(ent, glm::vec3(1.0f, 1.0f, 1.0f), ZERO, glm::vec3(0.03f));
		const CLevelsOfDetails& lod = reg.emplace<CLevelsOfDetails>(ent, new LODData{
			{
				LODLevel{ASSET_MESH_MOSSY_ROCK2_LOD0, 3.0f * 3.0f},
				LODLevel{ASSET_MESH_MOSSY_ROCK2_LOD1, 4.0f * 4.0f},
				LODLevel{ASSET_MESH_MOSSY_ROCK2_LOD2, 8.0f * 8.0f},
				LODLevel{ASSET_MESH_MOSSY_ROCK2_LOD3, 10.0f * 10.0f},
				LODLevel{ASSET_MESH_MOSSY_ROCK2_LOD4, 12.0f * 12.0f},
			},4});
		reg.emplace<CMesh>(ent, AssetLoader::FindOrLoadMeshFromFile(lod.lodData->levels[lod.lodData->currentLevel].path));
		reg.emplace<CSimpleMaterial>(ent,
			&AssetLoader::GetTexture(ASSET_MESH_MOSSY_ROCK2_DIFFUSE),
			&AssetLoader::GetTexture(ASSET_MESH_MOSSY_ROCK2_NORMALS),
			&AssetLoader::GetTexture(ASSET_MESH_MOSSY_ROCK2_ROUGHNESS));
	}

	// TEST
	{
		const entt::entity ent = reg.create();
		reg.emplace<CTransform>(ent, glm::vec3(1.0f, 3.0f, 1.0f), ZERO, glm::vec3(0.03f));
		const CLevelsOfDetails& lod = reg.emplace<CLevelsOfDetails>(ent, new LODData{
			{
				LODLevel{ASSET_MESH_MOSSY_ROCK2_LOD0, 3.0f * 3.0f},
				LODLevel{ASSET_MESH_MOSSY_ROCK2_LOD1, 4.0f * 4.0f},
				LODLevel{ASSET_MESH_MOSSY_ROCK2_LOD2, 8.0f * 8.0f},
				LODLevel{ASSET_MESH_MOSSY_ROCK2_LOD3, 10.0f * 10.0f},
				LODLevel{ASSET_MESH_MOSSY_ROCK2_LOD4, 12.0f * 12.0f},
			},4 });
		reg.emplace<CMesh>(ent, AssetLoader::FindOrLoadMeshFromFile(lod.lodData->levels[lod.lodData->currentLevel].path));
		reg.emplace<CSimpleMaterial>(ent,
			&AssetLoader::GetTexture(ASSET_MESH_MOSSY_ROCK2_DIFFUSE),
			&AssetLoader::GetTexture(ASSET_MESH_MOSSY_ROCK2_NORMALS),
			&AssetLoader::GetTexture(ASSET_MESH_MOSSY_ROCK2_ROUGHNESS));
	}

	// 3. Create a directional light, using CDirectionalLight
	{
		const entt::entity ent = reg.create();
		reg.emplace<CTransform>(ent, glm::vec3(1.2f, 1.0f, 2.0f));
		reg.emplace<CDirectionalLight>(ent, Color{ 1.0f, 1.0f, 1.0f }, 1.0f);
	}
}

int main(const int argc, const char** argv)
{
	Core::GamePlatform gamePlatform{};
	//auto mesh = AssetLoader::FindOrLoadMeshFromFile(R"(.\assets\geometry\pillar1\Aset_structure_stone_M_selvF_High.cdat)");

	Scene gameScene{ "Simulation" };
	
	gameScene.RegisterSystem<OceanSurfaceSystem>();
	gameScene.RegisterSystem<PlayerControlsSystem>();
	gameScene.RegisterSystem<Audio::AudioSystem>();
	gameScene.RegisterSystem<Rendering::LODSystem>();
	gameScene.RegisterSystem<WorldGenerationSystem>();
	gameScene.RegisterSystem<Rendering::RenderingSystem>(gamePlatform);
	
	SetupTestScene(gameScene);

	gamePlatform.RunMainLoop([&gameScene]()
		{
			// CALLED ONCE PER FRAME.
 			gameScene.Update();
		});

	return EXIT_SUCCESS;
}