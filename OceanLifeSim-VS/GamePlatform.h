#pragma once

#include "Common.h"
#include "InputManager.h"

namespace Core
{
	/**
	 * \brief Abstracts the window and input management from the rest of the game.
	 */
	class GamePlatform
	{
	public:
		GamePlatform();
		/**
		 * \brief Runs the game's main loop, taking care of event handling,
		 * calling the game logic callback, and swapping the OpenGL buffers
		 * at every iteration.
		 * \param callback Called at every iteration of the main loop.
		 */
		void RunMainLoop(std::function<void(void)>&& callback);
		void ExitGame() const;

		[[nodiscard]] GLFWwindow* GetWindow() const;

		template<typename T>
		[[nodiscard]] T GetWidth() const
		{
			return static_cast<T>(m_bufferWidth);
		}

		template<typename T>
		[[nodiscard]] T GetHeight() const
		{
			return static_cast<T>(m_bufferHeight);
		}

		[[nodiscard]] float GetAspectRatio() const;
		

	private:
		void OnFrameBufferResized(int width, int height);

		/**
		 * \brief Window owning the surface used for rendering, and for which
		 * input events are handled.
		 */
		GLFWwindow* m_window;
		/**
		 * \brief Responsible for storing which keys are pressed over time,
		 * as well as handling mouse movements.
		 */
		InputManager m_inputManager;

		int m_bufferWidth;
		int m_bufferHeight;
	};
}